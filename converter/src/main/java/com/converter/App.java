package com.converter;

import com.converter.pages.PageController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.stage.Window;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.FilenameUtils;

/**
 * JavaFX App
 * --module-path E:/javafx-sdk-18.0.1/lib --add-modules javafx.controls,javafx.fxml
 */
public class App extends Application {
    // Scene
    private static Scene scene;
    // Global css
    private static String css;

    /**
     * Start the application
     */
    @Override
    public void start(Stage stage) throws IOException {
        Thread.setDefaultUncaughtExceptionHandler(Log::handleException);
        Application.setUserAgentStylesheet(Application.STYLESHEET_MODENA);

        // Apply config to app
        applyGeneralConfig();


        scene = new Scene(loadFXML("convert"), 640, 480);
        // scene = new Scene(loadFXML("convert"), 640, 480);
        stage.setScene(scene);
        stage.setResizable(false);
        stage.setTitle("Excel to Word converter");
        stage.setHeight(590);
        stage.getIcons().add(new Image(App.class.getResourceAsStream("/com/converter/images/duck.png")));
        stage.show();

        // Setup
        PageController.setup();
        css = App.class.getResource("global.css").toExternalForm();
        scene.getStylesheets().add(css);
    }

    /**
     * Get ImageView with extension
     *
     * @param name
     * @return
     */
    public static ImageView getImageView(String name, String ext) {
        return new ImageView(new Image(App.class.getResourceAsStream("/com/converter/images/" + name + "." + ext)));
    }

    /**
     * Get ImageView
     *
     * @param name
     * @return
     */
    public static ImageView getImageView(String name) {
        return getImageView(name, "png");
    }



    /**
     * Get ImageView with size
     *
     * @param name
     * @param size
     * @return
     */
    public static ImageView getImageView(String name, double size) {
        // Create holder
        ImageView imageView = getImageView(name, "png");

        // Set size
        imageView.setFitHeight(size);
        imageView.setFitWidth(size);

        return imageView;
    }

    /**
     * Get resource as stream
     *
     * @param resource
     * @return
     */
    public static InputStream getResourceAsStream(String resource) {
        return App.class.getResourceAsStream(resource);
    }

    /**
     * Get jar file current directory
     *
     * @return
     */
    public static String getCurrentDirectory() {
        return FilenameUtils.getPathNoEndSeparator(App.class.getResource("").getFile());
    }

    /**
     * Set new root (active scene)
     *
     * @param fxml
     * @throws IOException
     */
    public static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    /**
     * Get current window
     *
     * @return
     */
    public static Window getWindow() {
        return scene.getWindow();
    }

    /**
     * Main method
     *
     * @param args
     */
    public static void main(String[] args) {
        launch();
    }

    //********************************************************************************
    // Private
    //********************************************************************************

    /**
     * Apply config to all resources
     */
    private void applyGeneralConfig() {
        // Read config
        Config.open();

        // Set locale
        Translator.setLocale(Config.get("LOCALE", "en_US"));
    }

    /**
     * Load fxml file
     *
     * @param fxml
     * @return
     * @throws IOException
     */
    private static Parent loadFXML(String fxml) throws IOException {
        // FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("scenes" + File.separator + fxml + ".fxml"));
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));

        fxmlLoader.setResources(Translator.getBundle());
        return fxmlLoader.load();
    }
}
