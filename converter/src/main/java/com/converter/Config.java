package com.converter;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Configuration class for write, read config
 */
public class Config {
    // Config file location
    private static String configFile = null;
    // Open control flag
    private static boolean isInstantiated = false;
    // Configuration map
    private static HashMap<String, String> configMap = new HashMap<>();
    // Temporary configuration map
    private static HashMap<String, String> tempConfigMap = new HashMap<>();

    /**
     * Get a configuration
     * 
     * @param key
     */
    public static String get(String key) {
        // Create holder
        String value;

        // Check if temporary configuration map contains key
        if (tempConfigMap.containsKey(key)) {
            value = tempConfigMap.get(key);
        } else {
            value = configMap.get(key);
        }

        return value;
    }

    /**
     * Safe get a configuration. If not exist, return default
     * 
     * @param key
     * @param default
     */
    public static String get(String key, String defaultValue) {
        return configMap.containsKey(key) || tempConfigMap.containsKey(key) ? get(key) : defaultValue;
    }

    /**
     * Set a configuration
     * 
     * @param key
     * @param value
     */
    public static String set(String key, String value) {
        // Replace if key exist, else put it in
        return tempConfigMap.containsKey(key) ? tempConfigMap.replace(key, value) : tempConfigMap.put(key, value);
    }

    /**
     * Check if a key's value is meaningful
     * 
     * @param key
     * @return
     */
    public static boolean isSet(String key) {
        // Create holder
        String value;
        boolean isSet;

        // Get value with default
        value = get(key, null);

        // Check if value is null
        if (value == null) {
            isSet = false;
        } else {
            isSet = true;
        }

        return isSet;
    }

    /**
     * Check if config is modified
     * 
     * @return
     */
    public static boolean isModified() {
        // If tempConfigMap is not empty, so it's modified
        return !tempConfigMap.isEmpty();
    }

    /**
     * Instantiate config file
     */
    public static void open() {
        // Get current directory
        configFile = System.getProperty("user.dir") + File.separator + "config.cfg";
        // Set flag
        isInstantiated = true;

        // Read config file
        read();
    }

    /**
     * Read configuration from path
     */
    public static void read() {
        try {
            // Check if config is instantiate
            if (!isInstantiated) {
                return;
            }

            // Create File Reader
            FileReader fr = new FileReader(configFile);
            // Create Buffered Reader
            BufferedReader br = new BufferedReader(fr);
            
            // Read lines
            for (String line; (line = br.readLine()) != null; ) {
                // Split key and value pair
                String[] pair = line.split("=");
                // Add to hashmap
                configMap.put(pair[0], pair.length == 1 ? null : pair[1]);
            }

            // Clear temp
            tempConfigMap.clear();
            
            // Close resources
            br.close();
            fr.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Write config to file
     */
    public static void write() {
        try {
            // Check if config is instantiate
            if (!isInstantiated) {
                return;
            }

            // Create FileWriter
            FileWriter fw = new FileWriter(configFile, false);
            // Create Buffered Reader
            BufferedWriter bw = new BufferedWriter(fw);
            
            // Iterate through map
            for (Map.Entry<String, String> entry : configMap.entrySet()) {
                // Create holder
                String data = entry.getKey() + "=";

                // Check if config is set
                if (tempConfigMap.containsKey(entry.getKey())) {
                    // Write line
                    data += tempConfigMap.get(entry.getKey());
                } else {
                    // Write line
                    data += entry.getValue();
                }
                
                // Write line
                bw.write(data);
                // Append new line
                bw.newLine();
            }

            // Flush reader
            bw.flush();
            // Close resources
            bw.close();
            fw.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Print map to screen
     */
    public static void printMain() {
        // Iterate through map
        for (Map.Entry<String, String> entry : configMap.entrySet()) {
            // Print
            System.out.println (entry.getKey() + " => " + entry.getValue());
        }
    }

    /**
     * Print map to screen
     */
    public static void printTemp() {
        // Iterate through map
        for (Map.Entry<String, String> entry : tempConfigMap.entrySet()) {
            // Print
            System.out.println (entry.getKey() + " => " + entry.getValue());
        }
    }
}
