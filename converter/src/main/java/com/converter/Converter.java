package com.converter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import com.converter.structure.*;

import exceptions.FillRedundantColumnException;
import exceptions.MissingRequiredColumnException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.Row.MissingCellPolicy;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

/**
 * Converter Class
 */
public class Converter {
    /**
     * Input file
     */
    private String from;
    // ***********************
    // Excel file data
    // Demonstration of structure: BookTable < LinesPack < CellsPack < StringsPack < BookCell > > > >
    // ***********************
    private BookTable data;
    /**
     * Word document instance
     */
    private XWPFDocument document;

    // ******************************
    // Constants
    // ******************************
    /**
     * SHUFFLE_NONE
     */
    public static final int SHUFFLE_NONE = 1;
    /**
     * SHUFFLE_QUESTION
     */
    public static final int SHUFFLE_QUESTION = 2;
    /**
     * SHUFFLE_ANSWER
     */
    public static final int SHUFFLE_ANSWER = 3;
    /**
     * SHUFFLE_ALL
     */
    public static final int SHUFFLE_ALL = 3;

    /**
     * Type: Section
     */
    static final String TYPE_SECTION = "Section";
    /**
     * Type: Question
     */
    static final String TYPE_QUESTION = "Question";
    /**
     * Type: Paragraph
     */
    static final String TYPE_PARAGRAPH = "Paragraph";
    /**
     * Type: Fill in the blank
     */
    static final String TYPE_FILL = "Fill";
    /**
     * Type: Word
     */
    static final String TYPE_WORD = "Word";
    /**
     * Type: Transformation
     */
    static final String TYPE_TRANSFORMATION = "Transformation";
    /**
     * Type: Writing
     */
    static final String TYPE_WRITING = "Writing";
    /**
     * Type: Yes/No
     */
    static final String TYPE_OR = "Yes/No";
    /**
     * Shuffleable type
     */
    static final List<String> SHUFFLEABLE_TYPE = Arrays.asList(TYPE_QUESTION, TYPE_WORD, TYPE_TRANSFORMATION, TYPE_OR);

    /**
     * Constructor
     */
    public Converter(String from) {
        this.from = from;
        this.data = new BookTable();
    }

    /**
     * Read the data in xlsx file
     */
    public void read() {
        try {
            // Create File instance
            File file = new File(this.from);
            // Create input stream instance
            FileInputStream iStream = new FileInputStream(file);
            // Create Workbook instance
            Workbook workbook = new XSSFWorkbook(file);

            // Analyze data
            analyze(workbook);

            // Close resources
            workbook.close();
            iStream.close();
        } catch (InvalidFormatException | IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Convert csv row into multiple choice in word
     */
    public void run() throws MissingRequiredColumnException, FillRedundantColumnException {
        // Create Document instance
        Document document = new Document();
        // Make new document
        document.make(this.data.unpack());
        // Get document
        this.document = document.getDocument();
        // Close Document handler
        document.close();
    }

    /**
     * Build docx file
     */
    public void build(String output) {
        try {
            // Create File instance
            File file = new File(output);
            // Create input stream instance
            FileOutputStream oStream = new FileOutputStream(file);

            // Write docx file with oStream
            document.write(oStream);

            // Close resources
            document.close();
            oStream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Delete all resource
     */
    public void close() {
        data = null;
        document = null;
    }

    /**
     * Shuffle data
     */
    public void shuffle(int option) {
        // Preserve index flag
        Integer preserveIndex = null;
        Integer originalIndex = null;
        // Control flag
        boolean shouldShuffleQuestion = SHUFFLE_QUESTION == option || SHUFFLE_ALL == option;
        boolean shouldShuffleAnswer = SHUFFLE_ANSWER == option || SHUFFLE_ALL == option;

        if (SHUFFLE_NONE == option) {
            return;
        }

        // Iterate through BookTable
        for (LinesPack linesPack : this.data) {
            // Iterate through LinesPack
            for (BookLine bookLine : linesPack) {
                // Iterate through CellsPack
                for (CellsPack cellsPack : bookLine) {
                    if (preserveIndex != null) {
                        // *****************************************************
                        // If an index is preserve, that mean
                        // this is the last cells pack (AKA the answer column).
                        // This column contain the right answer (or the index of)
                        // right answer column in CSV file. If the Question is
                        // supposed to be shuffled, so it right answer index
                        // maybe changed. So we must preserve it and trace the
                        // new one after shuffling
                        // *****************************************************

                        // Restore the new index
                        cellsPack.get(0).getValue().getText(0).setString(Integer.toString(preserveIndex + 1));
                        // Reset preserve index flag
                        preserveIndex = null;
                    } else if (cellsPack.isShuffleable() && shouldShuffleAnswer) {
                        // Get the original index
                        originalIndex = Integer.parseInt(bookLine.getLast().get(0).getValue().toString()) - 1;
                        // Check if cellsPack is shuffleable, then do shuffle
                        preserveIndex = cellsPack.shuffle(originalIndex);
                    }
                }
            }

            // Check if linesPack is shuffleable, then do shuffle
            if (linesPack.isShuffleable() && shouldShuffleQuestion) {
                linesPack.shuffle();
            }
        }
    }

    // *************************
    // Private
    // *************************

    /**
     * Analyze data to BookTable
     *
     * @param workbook
     */
    private void analyze(Workbook workbook) {
        // Assign table after analyze
        this.data = analyzeEachRow(workbook, workbook.getSheetAt(0));
    }

    /**
     * Analyze each row of the sheet
     *
     * @param workbook
     * @param sheet
     * @return
     */
    private BookTable analyzeEachRow(Workbook workbook, Sheet sheet) {
        // Create holder
        LinesPack linesPack = null;
        BookTable table = new BookTable();
        String latestType = "";
        String currentType;

        // Iterate through sheet row
        for (Row row : sheet) {
            // Get first row for type
            currentType = row.getCell(0, MissingCellPolicy.RETURN_BLANK_AS_NULL).getStringCellValue();
            // Detect if new section is reached, so new lines pack will be created
            if (!latestType.equals(currentType)) {
                // Reassign latestType
                latestType = currentType;

                // Create new line packs
                linesPack = new LinesPack();

                // Allow lines pack to shuffle if row type is QUESTION
                linesPack.setIsShuffleable(isAllowedToShuffle(latestType));
                // Add lines pack to book table
                table.add(linesPack);
            }

            // Add new book line to lines pack
            linesPack.add(analyzeEachCell(workbook, row, latestType));
        }

        return table;
    }

    /**
     * Analyze each cell of the row
     *
     * @param workbook
     * @param row
     * @param rowType
     * @return
     */
    private BookLine analyzeEachCell(Workbook workbook, Row row, String rowType) {
        // Create holder
        BookLine bookLine = new BookLine();
        CellsPack cellsPack = null;
        BookCell bookCell;
        int counter = 0;
        boolean isAnswer;

        // Analyze cell and make cells pack
        for (Cell cell : row) {
            // The answer part will lie between and include third cell and seventh cell
            // (second and fifth sixth in array)
            isAnswer = (counter == 2 || counter == 6) && TYPE_QUESTION.equals(rowType);

            // Check if new cells pack should be created
            if (counter == 0 || isAnswer) {
                // Create new cells pack
                cellsPack = new CellsPack();

                // Allow cells pack to shuffle if between second cell and sixth cell
                cellsPack.setIsShuffleable(counter == 2);
                // Add cells pack to book line
                bookLine.add(cellsPack);
            }

            // Convert cell content to formatted string
            bookCell = extractBookCell(workbook, cell);
            // Check for null
            if (bookCell == null) {
                // Continue to next cell
                continue;
            }

            // Add formatted string to cells pack
            cellsPack.add(bookCell);

            // Increase counter
            counter++;
        }

        return bookLine;
    }

    /**
     * Extract formatted string from cell
     *
     * @param workbook
     * @param cell
     * @return
     */
    private BookCell extractBookCell(Workbook workbook, Cell cell) {
        // Create holder
        FormattedString formattedString;
        String address = cell.getAddress().toString();
        Font externalFont = workbook.getFontAt(cell.getCellStyle().getFontIndex());

        // Check for type
        switch (cell.getCellType().toString()) {
            // Get cell value
            case "STRING":
                // This is string
                formattedString = extractFormat(
                    cell.getRichStringCellValue(),
                    externalFont
                );
                break;
            case "NUMERIC":
                if (cell.getNumericCellValue() % 1 == 0) {
                    // This is int
                    formattedString = extractFormat(
                        Integer.toString((int) cell.getNumericCellValue()),
                        externalFont
                    );
                } else {
                    // This is double
                    formattedString = extractFormat(
                        Double.toString(cell.getNumericCellValue()),
                        externalFont
                    );
                }
                break;
            default:
                return null;
        }

        return new BookCell(address, formattedString);
    }

    /**
     * Accept RichTextString and return FormattedString
     *
     * @param richTextString
     * @return
     */
    private FormattedString extractFormat(RichTextString richTextString, Font externalFont) {
        // Create holder
        XSSFRichTextString text = (XSSFRichTextString) richTextString;
        XSSFFont font = (XSSFFont) externalFont;

        // Get the total size of formatting run (run that has format)
        int formattingRunSize = text.numFormattingRuns();

        // **********************************
        // If formattingRunSize == 0, then use external font instead. Else, there are
        // multiple formatting run
        // **********************************
        return formattingRunSize == 0
            ? createFSFromRichTextString(text, font)
            : createFSFromRichTextString(text, formattingRunSize, font);
    }

    /**
     * Accept Number and return FormattedString
     *
     * @param string
     * @param externalFont
     * @return
     */
    private FormattedString extractFormat(String string, Font externalFont) {
        // Create holder
        FormattedString formattedString = new FormattedString();
        XSSFFont font = (XSSFFont) externalFont;

        // Add string to list
        formattedString.addText(createFTFromFont(string, font));

        return formattedString;
    }

    /**
     * Create new FormattedText and apply XSSFFont
     *
     * @param content
     * @param font
     * @return
     */
    private FormattedText createFTFromFont(String content, XSSFFont font) {
        // Initiate
        FormattedText formattedText = new FormattedText();
        // Set FormattedString string
        formattedText.setString(content);

        // If font is null, then there is no format
        if (font != null) {
            // Get bold
            formattedText.setIsBold(font.getBold());
            // Get italic
            formattedText.setIsItalic(font.getItalic());
            // Get underline
            formattedText.setIsUnderline(font.getUnderline() == 1);
            // System.out.println(content + " " + font.getUnderline());
            // Get size
            formattedText.setSize(font.getFontHeightInPoints());
            // Get color
            XSSFColor hexRGB = font.getXSSFColor();
            formattedText.setColor(hexRGB == null ? "00000000" : hexRGB.getARGBHex());

        }

        return formattedText;
    }

    /**
     * Create formatted string from XSSFRichTextString and it's font
     *
     * @param text
     * @param size
     * @return
     */
    private FormattedString createFSFromRichTextString(XSSFRichTextString text, int size, XSSFFont defaultFont) {
        // Create holder
        XSSFFont font;
        FormattedString formattedString = new FormattedString();
        FormattedText formattedText;

        // Loop through all formatting run
        for (int i = 0; i < size; i++) {
            // Get startIndex of string is held by formatting run in the origin string
            int startIndex = text.getIndexOfFormattingRun(i);
            // Get length of the string is held by formatting run
            int length = text.getLengthOfFormattingRun(i);

            // The string is held by formatting run will be the substring of the origin
            // string from "startIdx" and take "length" characters ("startIdx" is included)
            String content = text.getString().substring(startIndex, startIndex + length);
            // Get Font detail of formatting run and set FormattedString with font info
            font = text.getFontOfFormattingRun(i);

            // Create FormattedText
            formattedText = createFTFromFont(content, font == null ? defaultFont : font);

            // Add FormattedText to FormattedString
            formattedString.addText(formattedText);
        }

        return formattedString;
    }

    /**
     * Create formatted string from XSSFRichTextString and an external font
     *
     * @param text
     * @param externalFont
     * @return
     */
    private FormattedString createFSFromRichTextString(XSSFRichTextString text, XSSFFont externalFont) {
        // Create holder
        FormattedString formattedString = new FormattedString();
        FormattedText formattedText;

        // Create FormattedText
        formattedText = createFTFromFont(text.getString(), externalFont);

        // Add FormattedText to FormattedString
        formattedString.addText(formattedText);

        return formattedString;
    }

    /**
     * Check if type is allowed to be shuffle
     *
     * @param type
     * @return
     */
    private boolean isAllowedToShuffle(String type) {
        return SHUFFLEABLE_TYPE.contains(type);
    }
}