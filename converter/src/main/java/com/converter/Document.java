package com.converter;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.converter.structure.BookCell;
import com.converter.structure.FormattedString;
import com.converter.structure.FormattedText;
import com.google.common.base.Strings;

import exceptions.FillRedundantColumnException;
import exceptions.MissingRequiredColumnException;
import exceptions.ValidationException;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.apache.poi.xwpf.usermodel.XWPFTable.XWPFBorderType;
import org.apache.poi.xwpf.usermodel.XWPFTableCell.XWPFVertAlign;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.TableRowAlign;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTColumns;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTPageMar;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSectPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STSectionMark;

public class Document {
    /**
     * Header table
     */
    private XWPFTable table;
    /**
     * Word document instance
     */
    private XWPFDocument document;
    /**
     * Helper
     */
    private DocumentHelper helper;

    public Document() {
        // Initiate new document
        this.document = new XWPFDocument();
        this.helper = new DocumentHelper(this.document);
    }

    /**
     * Make new XWPFDocument with provided data
     *
     * @param data
     */
    public void make(ArrayList<ArrayList<BookCell>> data) throws MissingRequiredColumnException, FillRedundantColumnException {
        // Setup document
        setupDocument();
        // Create header
        createHeader();
        // Create body
        createBody(data);
        // Close document
        closeDocument();
    }

    public XWPFDocument getDocument() {
        return this.document;
    }

    /**
     * Clear all resource
     */
    public void close() {
        table = null;
        document = null;
        helper = null;
    }

    /**
     * Create header table
     */
    private void createHeader() {
        // Initialize variables
        XWPFTableRow row;
        // Create table
        this.table = this.document.createTable(1, 2);
        // Config table
        configHeaderTable();

        // Get row
        row = this.table.getRow(0);

        // Set data to left column
        createLeftTableHeader(row.getCell(0));
        // Set data to right column
        createRightTableHeader(row.getCell(1));
    }

    /**
     * Create body
     * FormattedString will be provided as an ArrayList. This means it is list of
     * part that combined into original string
     *
     * @param rows
     */
    @SuppressWarnings("unchecked")
    private void createBody(ArrayList<ArrayList<BookCell>> rows) throws MissingRequiredColumnException, FillRedundantColumnException {
        // Create holder
        ArrayList<FormattedString> cleanData;
        // Counter
        int sectionIndex = 1;
        int manualIndex = 1;

        // Construct body with provided data
        for (int globalIndex = 0; globalIndex < rows.size(); globalIndex++) {
            // Clone row to preserve the origin
            ArrayList<BookCell> cloned = (ArrayList<BookCell>) rows.get(globalIndex).clone();

            // Treat based on type
            switch (cloned.remove(0).getValue().toString()) {
                case Converter.TYPE_SECTION:
                    // Validate as Section
                    cleanData = Validator.validateSection(cloned, globalIndex);
                    // Treat as Section
                    sectionIndex = createSection(sectionIndex, cleanData);
                    break;
                case Converter.TYPE_QUESTION:
                    // Validate as Section
                    cleanData = Validator.validateQuestion(cloned, globalIndex);
                    // Treat as Question
                    manualIndex = createQuestion(manualIndex, cleanData);
                    break;
                case Converter.TYPE_PARAGRAPH:
                    // Validate as Section
                    cleanData = Validator.validateParagraph(cloned, globalIndex);
                    // Treat as Paragraph
                    manualIndex = createParagraph(manualIndex, cleanData);
                    break;
                case Converter.TYPE_TRANSFORMATION:
                    // Validate as Section
                    cleanData = Validator.validateTransformation(cloned, globalIndex);
                    // Treat as Transformation
                    manualIndex = createTransformation(manualIndex, cleanData);
                    break;
                case Converter.TYPE_WORD:
                    // Validate as Section
                    cleanData = Validator.validateWord(cloned, globalIndex);
                    // Treat as Word
                    manualIndex = createWord(manualIndex, cleanData);
                    break;
                case Converter.TYPE_FILL:
                    // Validate as Section
                    cleanData = Validator.validateFill(cloned, globalIndex);
                    // Treat as Fill in the blank
                    manualIndex = createFill(manualIndex, cleanData);
                    break;
                case Converter.TYPE_WRITING:
                    // Validate as Section
                    cleanData = Validator.validateWriting(cloned, globalIndex);
                    // Treat as Writing
                    manualIndex = createWriting(manualIndex, cleanData);
                    break;
                case Converter.TYPE_OR:
                    // Validate as Section
                    cleanData = Validator.validateOr(cloned, globalIndex);
                    // Treat as Yes/No
                    manualIndex = createOr(manualIndex, cleanData);
                    break;
            }
        }
    }

    /**
     * Create a Section
     *
     * @param index
     * @param row
     * @return
     */
    private int createSection(int index, ArrayList<FormattedString> row) {
        // Create holder
        XWPFParagraph paragraph;
        StringBuilder string = new StringBuilder();

        // Add index of section
        string.append("Part " + index + ". ");

        // Get string and ignore format
        for (FormattedString cell : row) {
            for (FormattedText text : cell) {
                string.append(text.getString());
            }
        }
        // Spacing
        this.helper.createParagraph();

        // Add index of section and setup
        paragraph = this.helper.createParagraph(string.toString(), true);
        paragraph.setSpacingAfter(20 * 8);
        paragraph.setSpacingBefore(20 * 8);

        return index + 1;
    }

    /**
     * Create a Question
     *
     * @param index
     * @param row
     * @return
     */
    private int createQuestion(int index, ArrayList<FormattedString> row) {
        // Create holder
        XWPFParagraph paragraph;

        // Concate content
        String indexer = "Question " + index + ". ";

        // Add question indexer
        paragraph = this.helper.createParagraph(indexer, true);

        // Add question content
        this.helper.applyFormattedStringToParagraph(row.remove(0), paragraph);

        // Setup paragraph
        this.helper.addSpaceBefore(paragraph, "420");

        // Create question's answers
        createAnswer(row, false);

        return index + 1;
    }

    /**
     * Create answer for question
     *
     * @param row
     * @param isBinary
     */
    private void createAnswer(ArrayList<FormattedString> row, boolean isBinary) {
        // Create holder
        XWPFParagraph paragraph;
        int[] pair;
        FormattedString formattedString;

        // Check if this process create answer for question or binary question (or)
        if (isBinary) {
            // Binary question will always be 1 row and 2 columns
            pair = new int[]{1, 2};
        } else {
            ArrayList<String> plainString = new ArrayList<>();

            // Get plain string (without format)
            for (FormattedString cell : row) {
                for (FormattedText text : cell) {
                    // Add plain string
                    plainString.add(text.getString());
                }
            }

            // Get colPerRow
            pair = this.helper.getRowColPair(plainString);
        }

        // Open column section
        ArrayList<Integer> breakpoints = this.helper.openColumns();

        // Create numbering
        BigInteger numID = this.helper.createNumbering();

        // Add answer base on pair
        for (int pRow = 0; pRow < pair[0]; pRow++) {
            for (int pCol = 0; pCol < pair[1]; pCol++) {
                // Create paragraph
                paragraph = this.helper.createParagraph();

                // Get FormattedString
                formattedString = row.get(pRow + pCol);

                // Add question answer to paragraph
                this.helper.applyFormattedStringToParagraph(formattedString, paragraph);

                // Add numbering and setup
                paragraph.setNumID(numID);
                paragraph.setNumILvl(BigInteger.valueOf(0));
                // Add space and setup
                this.helper.addSpaceBefore(paragraph, "312");
            }

            if (pRow < pair[0] - 1) {
                breakpoints = this.helper.breakColumns(breakpoints);
            }
        }

        // Get the right answer index at the last cell
        int rightAnswer = Integer.parseInt(row.remove(row.size() - 1).getText(0).getString()) - 1;

        // Close column section
        List<XWPFParagraph> paragraphs = this.helper.closeColumns(breakpoints);

        // Format right answer
        paragraph = paragraphs.get(rightAnswer);
        // Set number color
        paragraph.getCTP().getPPr().addNewRPr().addNewColor().setVal("FF0000");
        // Set content color
        paragraph.getRuns().get(0).setColor("FF0000");
    }

    /**
     * Create a Paragraph
     *
     * @param index
     * @param row
     * @return
     */
    private int createParagraph(int index, ArrayList<FormattedString> row) {
        // Create title
        createParagraphTitle(row.remove(0));

        // Add question indexer
        createParagraphBody(row, false);

        return index;
    }

    /**
     * Create paragraph title
     *
     * @param formattedString
     */
    private void createParagraphTitle(FormattedString formattedString) {
        // Create holder
        XWPFParagraph paragraph;
        StringBuilder title = new StringBuilder();

        // Get title
        for (FormattedText text : formattedString) {
            title.append(text.getString());
        }

        // Check if no title is specify
        if (!title.toString().toLowerCase().equals("[null]")) {
            // Create paragraph for title
            paragraph = this.helper.createParagraph(title.toString(), true);
            paragraph.setAlignment(ParagraphAlignment.CENTER);
            paragraph.setSpacingBetween(1.08);
            paragraph.setSpacingAfter(8 * 20);
        }
    }

    /**
     * Create paragraph body
     *
     * @param row
     */
    private void createParagraphBody(ArrayList<FormattedString> row, boolean isFillable) {
        // Create holder
        XWPFParagraph paragraph;
        HashMap<String, String> replaceMap = new HashMap<>();
        String spaceChar = getSpaceChar("FILL");

        // Loop through all formatted string in cell
        for (FormattedString cell : row) {
            // Create paragraph
            paragraph = this.helper.createParagraph();
            // Format paragraph
            paragraph.setSpacingBetween(isFillable ? 1.25 : 1.08);
            paragraph.setSpacingAfter(8 * 20);
            paragraph.setAlignment(ParagraphAlignment.BOTH);

            // Check if paragraph is fillable
            if (isFillable) {
                // Create replaceMap
                replaceMap.put("\\([\\d\\s]+\\)", spaceChar);
                // Apply formatted string to paragraph
                this.helper.applyFormattedStringToParagraph(cell, paragraph, replaceMap);
            } else {
                // Apply formatted string to paragraph
                this.helper.applyFormattedStringToParagraph(cell, paragraph);
            }
        }
    }

    /**
     * Create transformation
     *
     * @param index
     * @param row
     * @return
     */
    private int createTransformation(int index, ArrayList<FormattedString> row) {
        // Create holder
        XWPFParagraph paragraph;
        String indexer = "Question " + index + ". ";

        // Add question indexer
        paragraph = this.helper.createParagraph(indexer, true);
        // Setup paragraph
        paragraph = this.helper.addSpaceBefore(paragraph, "420");

        // First FormattedString is the transform content
        this.helper.applyFormattedStringToParagraph(row.get(0), paragraph);

        // Add answer section
        paragraph = this.helper.createEquation("=>");
        // Setup paragraph
        paragraph = this.helper.addSpaceBefore(paragraph, "312");

        // Second FormattedString is the transform hint
        this.helper.applyFormattedStringToParagraph(row.get(1), paragraph);

        // Add tab leader
        this.helper.addTabLeader(paragraph);

        return index + 1;
    }

    /**
     * Create word
     *
     * @param index
     * @param row
     * @return
     */
    private int createWord(int index, ArrayList<FormattedString> row) {
        // Create holder
        XWPFParagraph paragraph;
        String indexer = "Question " + index + ". ";
        HashMap<String, String> replaceMap = new HashMap<>();
        String spaceChar = getSpaceChar("WORD");

        // Add question indexer
        paragraph = this.helper.createParagraph(indexer, true);
        // Setup paragraph
        paragraph = this.helper.addSpaceBefore(paragraph, "420");

        // Set replace pattern
        replaceMap.put("\\([\\w\\s]+\\)", spaceChar);

        // First FormattedString is the transform content
        this.helper.applyFormattedStringToParagraph(row.get(0), paragraph, replaceMap);

        return index + 1;
    }

    /**
     * Create fill in the blank
     *
     * @param index
     * @param row
     * @return
     */
    private int createFill(int index, ArrayList<FormattedString> row) {
        // Create title
        createParagraphTitle(row.remove(0));

        // Add question indexer
        createParagraphBody(row, true);

        return index;
    }

    /**
     * Create writing
     *
     * @param index
     * @param row
     * @return
     */
    private int createWriting(int index, ArrayList<FormattedString> row) {
        // Create holder
        XWPFParagraph paragraph;
        int rowNum;

        // Create new paragraph and setup tab leader
        paragraph = this.helper.createParagraph();
        paragraph = this.helper.addTabLeader(paragraph);
        paragraph.setSpacingBetween(1.25);
        // Get the number of floor. Each line will be able to contain 20 characters
        rowNum = Math.floorDiv(Integer.parseInt(row.get(0).toString()), 20);

        // Append tab leader [rowNum] times
        for (int i = 0; i < rowNum; i++) {
            // Add tab leader
            paragraph.createRun().addTab();
            // Linebreak
            paragraph.createRun().addBreak();
        }

        return index;
    }

    /**
     * Create or question
     *
     * @param index
     * @param row
     * @return
     */
    private int createOr(int index, ArrayList<FormattedString> row) {
        // Create holder
        XWPFParagraph paragraph;

        // Concate content
        String indexer = "Question " + index + ". ";

        // Add question indexer
        paragraph = this.helper.createParagraph(indexer, true);

        // Add question content
        this.helper.applyFormattedStringToParagraph(row.remove(0), paragraph);

        // Setup paragraph
        this.helper.addSpaceBefore(paragraph, "420");

        // Create question's answers
        createAnswer(row, true);

        return index + 1;
    }

    /**
     * Close the document (Must be call at the end)
     */
    private void closeDocument() {
        // Create holder
        CTSectPr ctSectPr;
        CTColumns ctColumns;
        CTPageMar ctPageMar;

        // Add new sectPr at the end of the document
        ctSectPr = this.document.getDocument().getBody().addNewSectPr();

        // Create type
        ctSectPr.addNewType().setVal(STSectionMark.CONTINUOUS);

        // Create cols and setup
        ctColumns = ctSectPr.addNewCols();
        ctColumns.setSpace("720");
        ctColumns.setNum(BigInteger.valueOf(1));

        // Create pageMar and setup
        ctPageMar = ctSectPr.addNewPgMar();
        ctPageMar.setTop("720");
        ctPageMar.setRight("720");
        ctPageMar.setBottom("720");
        ctPageMar.setLeft("720");
        ctPageMar.setHeader("720");
        ctPageMar.setFooter("720");
        ctPageMar.setGutter("0");
    }

    // **********************************
    // Private supporters
    // **********************************

    /**
     * Setup document
     */
    private void setupDocument() {
        // Create numbering
        this.document.createNumbering();
    }

    /**
     * Config header table
     */
    private void configHeaderTable() {
        // Set width
        this.table.setWidth(this.helper.inches(7.5));
        this.table.getRow(0).getCell(0).setWidth(this.helper.inches(3.1));
        this.table.getRow(0).getCell(1).setWidth(this.helper.inches(4.4));
        // Set alignment
        this.table.setTableAlignment(TableRowAlign.LEFT);
        this.table.getRow(0).getCell(0).setVerticalAlignment(XWPFVertAlign.TOP);
        this.table.getRow(0).getCell(0).setVerticalAlignment(XWPFVertAlign.TOP);
        // Set border
        this.table.setTopBorder(XWPFBorderType.NONE, 0, 0, "");
        this.table.setRightBorder(XWPFBorderType.NONE, 0, 0, "");
        this.table.setBottomBorder(XWPFBorderType.NONE, 0, 0, "");
        this.table.setLeftBorder(XWPFBorderType.NONE, 0, 0, "");
        this.table.setInsideVBorder(XWPFBorderType.NONE, 0, 0, "");
    }

    /**
     * Create left table header column
     *
     * @param cell
     */
    private void createLeftTableHeader(XWPFTableCell cell) {
        // Introduction line
        this.helper.createParagraph(cell.addParagraph(), "SỞ GIÁO DỤC VÀ ĐÀO TẠO")
            .setAlignment(ParagraphAlignment.CENTER);
        this.helper.createParagraph(cell.addParagraph(), "THÀNH PHỐ HỒ CHÍ MINH")
            .setAlignment(ParagraphAlignment.CENTER);
        this.helper.createParagraph(cell.addParagraph(), "TRUNG TÂM GDTX CHU VĂN AN ", 12, true)
            .setAlignment(ParagraphAlignment.CENTER);

        // Add line picture to seperate 2 first line
        this.helper.addImage(cell.addParagraph(), "line", 80, 1);

        // Create new paragraph for sub table and setup
        this.helper.createTextBox(cell.addParagraph(), "ĐỀ CHÍNH THỨC", 13, 132.65, 26.95)
            .setAlignment(ParagraphAlignment.CENTER);

        // Add the last line break
        this.helper.createLineBreak(cell.addParagraph());

        // Remove first linebreak
        cell.removeParagraph(0);
    }

    /**
     * Create right table header column
     *
     * @param cell
     */
    private void createRightTableHeader(XWPFTableCell cell) {
        // Create holder
        XWPFParagraph paragraph;

        // Set content
        this.helper.createParagraph(cell.addParagraph(), "ĐỀ KIỂM TRA GIỮA HỌC KỲ I NĂM HỌC 2021 – 2022", 12, true)
            .setAlignment(ParagraphAlignment.CENTER);
        this.helper.createParagraph(cell.addParagraph(), "Môn: TIẾNG ANH – Lớp: 11", 12, true)
            .setAlignment(ParagraphAlignment.CENTER);

        // Time note
        paragraph = cell.addParagraph();
        paragraph.setAlignment(ParagraphAlignment.CENTER);

        // Add time
        this.helper.createParagraphItalic(paragraph, "Thời gian:");
        // Add time value
        this.helper.createParagraphItalic(paragraph, " 50 phút ");
        // Add note
        this.helper.createParagraphItalic(paragraph, "(Không kể thời gian phát đề)");

        // Create linebreak
        this.helper.createLineBreak(cell.addParagraph());

        // Add test ID TextBox
        paragraph = this.helper.createTextBox(cell.addParagraph(), "Mã đề: 111", 11, 85.55, 26.95);
        paragraph.setAlignment(ParagraphAlignment.RIGHT);
        paragraph.setIndentationRight(200);

        // Remove first linebreak
        cell.removeParagraph(0);
    }

    /**
     * Get space character with type prefix
     *
     * @param prefix
     * @return
     */
    private String getSpaceChar(String prefix) {
        // Create holder
        StringBuilder spaceChar = new StringBuilder();

        // Get space character
        if ("underscore".equals(Config.get(prefix + "_SPACE_CHAR"))) {
            spaceChar.append(Strings.repeat("_", 15));
        } else if ("dot".equals(Config.get(prefix + "_SPACE_CHAR"))) {
            spaceChar.append(Strings.repeat(".", 15));
        } else {
            spaceChar.append(Strings.repeat("_", 15));
        }

        // Check for space position
        if ("after".equals(Config.get(prefix + "_SPACE_POS"))) {
            spaceChar.insert(0, "$self$ ");
        } else if ("before".equals(Config.get(prefix + "_SPACE_POS"))) {
            spaceChar.append(" $self$");
        } else {
            spaceChar.append(" $self$");
        }

        return spaceChar.toString();
    }
}
