package com.converter;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.converter.structure.FormattedString;
import com.converter.structure.FormattedText;
import com.microsoft.schemas.vml.CTGroup;
import com.microsoft.schemas.vml.CTShape;

import org.apache.commons.io.IOUtils;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.UnderlinePatterns;
import org.apache.poi.xwpf.usermodel.XWPFAbstractNum;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFNumbering;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.xmlbeans.XmlException;
import org.openxmlformats.schemas.drawingml.x2006.main.CTGraphicalObject;
import org.openxmlformats.schemas.drawingml.x2006.wordprocessingDrawing.CTAnchor;
import org.openxmlformats.schemas.officeDocument.x2006.math.CTOMath;
import org.openxmlformats.schemas.officeDocument.x2006.math.STStyle;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTAbstractNum;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTColumns;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTDrawing;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTFonts;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTInd;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTLongHexNumber;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTLvl;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTP;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTPPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTPPrGeneral;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTPageMar;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTPageSz;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTPicture;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTR;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTRPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSectPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSpacing;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTabStop;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STHint;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STJc;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STLineSpacingRule;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STNumberFormat;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STSectionMark;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STTabJc;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STTabTlc;

public class DocumentHelper {
    /** XWPFDocument instance */
    private XWPFDocument document;

    public DocumentHelper(XWPFDocument document) {
        this.document = document;
    }

    /**
     * Create empty paragraph
     * 
     * @return
     */
    public XWPFParagraph createParagraph() {
        return this.document.createParagraph();
    }

    /**
     * Create paragraph with provided XWPFParagraph
     * 
     * @param paragraph
     * @param content
     * @param size
     * @param isBold
     * @return
     */
    public XWPFParagraph createParagraph(XWPFParagraph paragraph, String content, int size, boolean isBold) {
        // Create new paragraph
        if (paragraph == null) {
            paragraph = this.document.createParagraph();
        }

        // Setup paragraph
        XWPFRun run = paragraph.createRun();
        run.setText(content);
        run.setFontFamily("Times New Roman");
        run.setFontSize(size);
        run.setBold(isBold);

        return paragraph;
    }

    /**
     * Create paragraph with provided XWPFParagraph and default font size is 12
     * 
     * @param paragraph
     * @param content
     * @param size
     * @param isBold
     * @return
     */
    public XWPFParagraph createParagraph(XWPFParagraph paragraph, String content) {
        return createParagraph(paragraph, content, 12, false);
    }

    /**
     * Create linebreak
     * 
     * @param paragraph
     * @return
     */
    public XWPFParagraph createLineBreak(XWPFParagraph paragraph) {
        return createParagraph(paragraph, "", 12, false);
    }

    /**
     * Create paragraph
     * 
     * @param content
     * @param size
     * @param isBold
     * @return
     */
    public XWPFParagraph createParagraph(String content, int size, boolean isBold) {
        return createParagraph(null, content, size, isBold);
    }

    /**
     * Create linebreak
     * 
     * @return
     */
    public XWPFParagraph createLineBreak() {
        return createParagraph("", 12, false);
    }

    /**
     * Create paragraph with default font size is 12
     * 
     * @param content
     * @return
     */
    public XWPFParagraph createParagraph(String content) {
        return createParagraph(content, 12, false);
    }

    /**
     * Create paragraph with default font size is 12 and isBold
     * 
     * @param content
     * @param isBold
     * @return
     */
    public XWPFParagraph createParagraph(String content, boolean isBold) {
        return createParagraph(content, 12, isBold);
    }

    /**
     * Add image. Required xml file for other setting. Only accept PNG
     * 
     * @param paragraph
     * @param path
     * @param type
     * @param width
     * @param height
     * @return
     */
    public XWPFParagraph addImage(XWPFParagraph paragraph, String name, int width, int height) {
        if (paragraph == null) {
            paragraph = this.document.createParagraph();
        }

        try {
            // Get Run
            XWPFRun run = paragraph.createRun();
            // Initiate variables
            String path = "/com/converter/images/" + name + ".png";
            String filename = name + ".png";
            width = Units.toEMU(width);
            height = Units.toEMU(height);

            // Add picture
            run.addPicture(getResourcesAsStream(path), XWPFDocument.PICTURE_TYPE_PNG, filename, width, height);

            // Get objects
            CTDrawing drawing = run.getCTR().getDrawingArray(0);
            CTGraphicalObject graphicalObject = drawing.getInlineArray(0).getGraphic();
            // Setup image setting with XML file
            String anchorXML = getResourcesAsString("/com/converter/xmls/" + name + ".xml");

            // Create new drawing with image XML
            CTDrawing newDrawing = CTDrawing.Factory.parse(anchorXML);
            // Get new array anchor
            CTAnchor anchor = newDrawing.getAnchorArray(0);
            // Set anchor with graphicalObject
            anchor.setGraphic(graphicalObject);
            // Set new anchor for drawing
            drawing.setAnchorArray(new CTAnchor[] { anchor });
            drawing.removeInline(0);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return paragraph;
    }

    /**
     * Add image. Required xml file for other setting. Only accept PNG
     * 
     * @param paragraph
     * @param path
     * @param type
     * @param width
     * @param height
     * @return
     */
    public XWPFParagraph addImage(String name, int width, int height) {
        return addImage(null, name, width, height);
    }

    /**
     * Add Text Box shape
     * 
     * @param paragraph
     * @param content
     * @param fontSize
     * @param width
     * @param height
     * @return
     */
    public XWPFParagraph createTextBox(XWPFParagraph paragraph, String content, int fontSize,
            double width, double height) {
        if (paragraph == null) {
            paragraph = this.document.createParagraph();
        }

        try {
            // Get Run
            XWPFRun run = paragraph.createRun();
            // Create CTGroup
            CTGroup ctGroup = CTGroup.Factory.newInstance();
            // Create CTShape and setup
            CTShape ctShape = ctGroup.addNewShape();
            ctShape.setStyle("width:" + width + "pt;height:" + height + "pt");
            ctShape.addNewStroke().setWeight("1.5pt");
            // Create new Text Box
            CTP textBoxCTP = ctShape.addNewTextbox().addNewTxbxContent().addNewP();
            // Create paragraph and run in Text Box and setup
            XWPFParagraph textBoxP = new XWPFParagraph(textBoxCTP, paragraph.getBody());
            textBoxP.setAlignment(ParagraphAlignment.CENTER);
            XWPFRun textBoxR = textBoxP.createRun();
            textBoxR.setText(content);
            textBoxR.setFontFamily("Times New Roman");
            textBoxR.setFontSize(fontSize);
            textBoxR.setBold(true);

            // Create new CTPicture
            CTPicture cTPicture = CTPicture.Factory.parse(ctGroup.getDomNode());
            // Get CTR of provided paragraph's run
            CTR cTR = run.getCTR();
            // Create new Pict
            cTR.addNewPict();
            // Add created CTPicture above into Pict array
            cTR.setPictArray(0, cTPicture);

            return paragraph;
        } catch (XmlException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Create paragraph with provided XWPFParagraph and set to Italic
     * 
     * @param paragraph
     * @param content
     * @param size
     * @param isBold
     * @return
     */
    public XWPFParagraph createParagraphItalic(XWPFParagraph paragraph, String content) {
        // Create new paragraph
        if (paragraph == null) {
            paragraph = this.document.createParagraph();
        }

        // Setup paragraph
        XWPFRun run = paragraph.createRun();
        run.setText(content);
        run.setFontFamily("Times New Roman");
        run.setFontSize(12);
        run.setItalic(true);

        return paragraph;
    }

    /**
     * Create paragraph set to Italic
     * 
     * @param paragraph
     * @param content
     * @param size
     * @param isBold
     * @return
     */
    public XWPFParagraph createParagraphItalic(String content) {
        return createParagraphItalic(null, content);
    }

    /**
     * Add Text Box shape
     * 
     * @param paragraph
     * @param content
     * @param width
     * @param height
     * @return
     */
    public XWPFParagraph createTextBox(String content, int fontSize, double width, double height) {
        return createTextBox(null, content, fontSize, width, height);
    }

    /**
     * Open column section and return breakpoints list
     * 
     * @return
     */
    public ArrayList<Integer> openColumns() {
        // Get the startPoint
        int startAt = this.document.getParagraphs().size();
        // Mark column section
        markColumn(1);

        // Create breakpoints
        ArrayList<Integer> breakpoints = new ArrayList<>();
        // Add start point
        breakpoints.add(startAt);

        return breakpoints;
    }

    /**
     * Return paragraph index where columns is open
     * 
     * @return
     */
    public ArrayList<Integer> breakColumns(ArrayList<Integer> breakpoints) {
        // This method will create a breakpoints so the column can be break in to
        // multiple line
        int breakAt = this.document.getParagraphs().size();
        // Get breakpoints last element index
        int lastBreakpointsIndex = breakpoints.size() - 1;
        // Calculate startAt or breakAt point
        int prevPoint = breakpoints.get(lastBreakpointsIndex);
        // Calculate the number of paragraph between points
        int size = breakAt - prevPoint - 1;

        // Mark column with new size
        markColumn(size);

        // Add break point
        breakpoints.add(breakAt);

        return breakpoints;
    }

    /**
     * Close the columns and return columns's paragraphs. The number of paragraph is
     * the range of breakpoints
     * 
     * @param size
     * @return
     */
    public ArrayList<XWPFParagraph> closeColumns(ArrayList<Integer> breakpoints) {
        // The size of paragraph in column (aka column size) will be the size of
        // paragraphs is created after openColumns is called and before closeColumns is
        // called.
        breakpoints = breakColumns(breakpoints);
        // Get start point
        int startAt = breakpoints.get(0);
        // Get breakpoints size
        int endAt = breakpoints.get(breakpoints.size() - 1);

        // Get list of paragraphs
        List<XWPFParagraph> paragraphs = this.document.getParagraphs();
        // Initiate return
        ArrayList<XWPFParagraph> returnParagraphs = new ArrayList<>();

        // Add all paragraph related to column to return
        // The order will be 1. left to right, 2. up to down
        for (int i = startAt + 1; i < endAt; i++) {
            // The index in breakpoints is column marker paragraph
            if (!breakpoints.contains(i)) {
                // The index of related paragraph will not contained in breakpoint
                returnParagraphs.add(paragraphs.get(i));
            }
        }

        // return returnParagraphs;
        return returnParagraphs;
    }

    /**
     * Create numbering list
     * 
     * @return
     */
    public BigInteger createNumbering() {
        // Create holders
        XWPFNumbering numbering;
        XWPFAbstractNum abstractNum;

        // Make sure document should have Numbering first
        if (this.document.getNumbering() == null) {
            this.document.createNumbering();
        }
        // Get Numbering
        numbering = this.document.getNumbering();

        // Create new ID base on the size of IDs
        BigInteger id = BigInteger.valueOf(numbering.getNums().size() + 1);

        // Create abstractNum
        abstractNum = new XWPFAbstractNum(createCtAbstractNum(id));
        numbering.addAbstractNum(abstractNum);

        // Add num
        numbering.addNum(id);

        return id;
    }

    /**
     * Create run
     * 
     * @param paragraph
     * @param content
     * @return
     */
    public XWPFRun createRun(XWPFParagraph paragraph, String content) {
        // Create run
        XWPFRun run = paragraph.createRun();

        // Setup
        run.setText(content);
        run.setFontFamily("Times New Roman");
        run.setFontSize(12);

        return run;
    }

    /**
     * Create equation
     * 
     * @param content
     * @return
     */
    public XWPFParagraph createEquation(String content) {
        // Create holder
        XWPFParagraph paragraph;
        CTOMath ctoMath;
        org.openxmlformats.schemas.officeDocument.x2006.math.CTR ctr;
        CTRPr ctrPr;
        CTFonts ctFonts;

        // Create paragraph
        paragraph = createParagraph();

        // Add oMath
        ctoMath = paragraph.getCTP().addNewOMath();
        // Add new r
        ctr = ctoMath.addNewR();
        {
            // Add new sty
            ctr.addNewRPr().addNewSty().setVal(STStyle.P);
            // Add new rPr
            ctrPr = ctr.addNewRPr2();

            {
                // Add new rFonts
                ctFonts = ctrPr.addNewRFonts();
                // Set rFonts
                ctFonts.setHint(STHint.DEFAULT);
                ctFonts.setAscii("Cambria Math");
                ctFonts.setHAnsi("Cambria Math");

                // Add new sz
                ctrPr.addNewSz().setVal(21);
                // Add new szCs
                ctrPr.addNewSzCs().setVal(22);
                // Add new lang
                ctrPr.addNewLang().setVal("en-US");
            }

            // Add new t
            ctr.addNewT().setStringValue(content);
        }

        // Add space after
        paragraph.createRun().setText(" ");

        return paragraph;
    }

    /**
     * Add space before for paragraph
     * 
     * @param paragraph
     * @param size
     * @return
     */
    public XWPFParagraph addSpaceBefore(XWPFParagraph paragraph, String size) {
        // Create holder
        CTSpacing ctSpacing;

        // Add space
        ctSpacing = paragraph.getCTP().addNewPPr().addNewSpacing();
        ctSpacing.setAfter("0");
        ctSpacing.setLine(size);
        ctSpacing.setLineRule(STLineSpacingRule.AT_LEAST);

        return paragraph;
    }

    /**
     * Add tab leader for paragraph
     * 
     * @param paragraph
     * @return
     */
    public XWPFParagraph addTabLeader(XWPFParagraph paragraph) {
        // Create holder
        CTPPr ctpPr;
        CTTabStop ctTabStop;

        // Get pPr
        if (paragraph.getCTP().isSetPPr()) {
            // Get pPr if is set
            ctpPr = paragraph.getCTP().getPPr();
        } else {
            // Add new if not
            ctpPr = paragraph.getCTP().addNewPPr();
        }

        // Add new keepNext
        ctpPr.addNewKeepNext().setVal(false);
        // Add new keepLines
        ctpPr.addNewKeepLines().setVal(false);
        // Add new pageBreakBefore
        ctpPr.addNewPageBreakBefore().setVal(false);
        // Add new windowControl
        ctpPr.addNewWidowControl();
        {
            // Add new tabs and tab
            ctTabStop = ctpPr.addNewTabs().addNewTab();

            // Setup tab
            ctTabStop.setVal(STTabJc.LEFT);
            if ("underscore".equals(Config.get("WRITING_SPACE_CHAR"))) {
                ctTabStop.setLeader(STTabTlc.UNDERSCORE);
            } else if ("dot".equals(Config.get("WRITING_SPACE_CHAR"))) {
                ctTabStop.setLeader(STTabTlc.DOT);
            } else {
                // Default will be underscore
                ctTabStop.setLeader(STTabTlc.UNDERSCORE);
            }
            ctTabStop.setPos("10920");
        }
        // Add new kinsoku
        ctpPr.addNewKinsoku();
        // Add new wordWrap
        ctpPr.addNewWordWrap();
        // Add new overflowPunct
        ctpPr.addNewOverflowPunct();
        // Add new topLinePunct
        ctpPr.addNewTopLinePunct().setVal(false);
        // Add new autoSpaceDE
        ctpPr.addNewAutoSpaceDE();
        // Add new autoSpaceDN
        ctpPr.addNewAutoSpaceDN();
        // Add new bidi
        ctpPr.addNewBidi().setVal(false);
        // Add new adjustRightInd
        ctpPr.addNewAdjustRightInd();
        // Add new snapToGrid
        ctpPr.addNewSnapToGrid();

        // Add tab to paragraph
        paragraph.createRun().addTab();

        return paragraph;
    }

    // *************************
    // Other
    // *************************

    /**
     * Convert value to inches
     * 
     * @param x
     * @return String
     */
    public String inches(double x) {
        return "" + (int) (x * 1440);
    }

    /**
     * Get InputStream from path
     * 
     * @param path
     */
    public InputStream getResourcesAsStream(String path) {
        return this.getClass().getResourceAsStream(path);
    }

    /**
     * Get resources file as string
     * 
     * @param path
     * @return
     */
    public String getResourcesAsString(String path) {
        try {
            return IOUtils.toString(getResourcesAsStream(path), StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";
    }

    /**
     * Get number of row and number of col of each row
     * 
     * @param string
     * @return
     */
    public int[] getRowColPair(List<String> row) {
        // Checker
        int biggestLength = 0;

        // Get the max length string
        for (String string : row) {
            biggestLength = string.length() > biggestLength ? string.length() : biggestLength;
        }

        // Decision
        if (biggestLength <= 20) {
            return new int[] { 1, 4 };
        } else if (biggestLength <= 40) {
            return new int[] { 2, 2 };
        } else {
            return new int[] { 4, 1 };
        }
    }

    /**
     * Apply FormattedString to Paragraph
     * 
     * @param formattedString
     * @param paragraph
     */
    public void applyFormattedStringToParagraph(FormattedString formattedString, XWPFParagraph paragraph) {
        // Create holder
        XWPFRun run;
        
        // First FormattedString is the transform content
        for (FormattedText text : formattedString) {
            // Create run and apply formatted string
            run = paragraph.createRun();
            applyFormattedTextToRun(text, run);
        }
    }

    /**
     * Apply FormattedString to Paragraph and replace based on patterns map
     * 
     * @param formattedString
     * @param paragraph
     * @param replaceMap
     */
    public void applyFormattedStringToParagraph(FormattedString formattedString, XWPFParagraph paragraph, HashMap<String, String> replaceMap) {
        // Create holder
        XWPFRun run;
        ArrayList<Pattern> patterns = new ArrayList<>();
        ArrayList<String> replacements = new ArrayList<>();
        Matcher matcher;
        String matchedString;
        String matchedReplacement;
        StringBuilder replacedString = new StringBuilder();

        // Iterate through map to retrieve patterns list
        for (HashMap.Entry<String, String> entry : replaceMap.entrySet()) {
            // Compile pattern with map key and add to list
            patterns.add(Pattern.compile(entry.getKey()));
            // Add init replacement for the pattern
            replacements.add(entry.getValue());
        }
        
        // First FormattedString is the transform content
        for (FormattedText text : formattedString) {
            // Use each pattern in patterns to match text
            for (int i = 0; i < replaceMap.size(); i++) {
                // Match text with pattern
                matcher = patterns.get(i).matcher(text.getString());

                // Iterate through all matched
                while (matcher.find()) {
                    // Get matchedString
                    matchedString = matcher.group();
                    // Replace &self& in corresponding replacement
                    matchedReplacement = replacements.get(i).replaceAll("\\$self\\$", matchedString);
                    // Replace the matched string with matched replacement
                    matcher.appendReplacement(replacedString, matchedReplacement);
                }

                // Append the rest
                matcher.appendTail(replacedString);

                // Set formatted text with replaced string
                text.setString(replacedString.toString());
            }
            
            // Create run and apply formatted string
            run = paragraph.createRun();
            applyFormattedTextToRun(text, run);
        }
    }

    /**
     * Apply FormattedText to Run
     * 
     * @param formattedText
     * @param run
     */
    public void applyFormattedTextToRun(FormattedText formattedText, XWPFRun run) {
        // Add text
        run.setText(formattedText.getString());
        run.setColor(formattedText.getColor());
        run.setFontFamily(formattedText.getFont());
        run.setFontSize(formattedText.getSize());
        run.setBold(formattedText.isBold());
        run.setItalic(formattedText.isItalic());
        run.setUnderline(formattedText.isUnderline() ? UnderlinePatterns.SINGLE : UnderlinePatterns.NONE);
    }

    // *************************
    // Private
    // *************************

    /**
     * Mark column section
     * 
     * @param
     */
    private void markColumn(int size) {
        // Create holders
        XWPFParagraph paragraph;
        CTPPr ctpPr;
        CTSectPr ctSectPr;
        CTPageSz ctPageSz;
        CTPageMar ctPageMar;
        CTColumns ctColumns;

        // Create new paragraph
        paragraph = this.document.createParagraph();
        // Add new pPr for paragraph
        ctpPr = paragraph.getCTP().addNewPPr();

        // Add sectPr
        ctSectPr = ctpPr.addNewSectPr();
        // Set type
        ctSectPr.addNewType().setVal(STSectionMark.CONTINUOUS);
        // Add pgSz to sectPr and setup
        ctPageSz = ctSectPr.addNewPgSz();
        ctPageSz.setW("12240");
        ctPageSz.setH("15840");
        // Add pgMar
        ctPageMar = ctSectPr.addNewPgMar();
        ctPageMar.setTop("720");
        ctPageMar.setRight("720");
        ctPageMar.setBottom("720");
        ctPageMar.setLeft("720");
        ctPageMar.setFooter("720");
        ctPageMar.setGutter("0");
        // Add cols
        ctColumns = ctSectPr.addNewCols();
        ctColumns.setSpace("720");
        ctColumns.setNum(BigInteger.valueOf(size));
    }

    /**
     * Create CTAbstractNum
     * 
     * @param id
     * @return
     */
    private CTAbstractNum createCtAbstractNum(BigInteger id) {
        // Create holders
        CTAbstractNum ctAbstractNum;
        CTLongHexNumber ctLongHexNumber;
        CTLvl ctLvl;
        CTPPrGeneral ctpPr;
        CTInd ctInd;
        CTRPr ctrpr;
        CTFonts ctFonts;

        // Create random byte[]
        byte[] bytes = new byte[8];
        new Random().nextBytes(bytes);
        // Create random LongHexNumber
        ctLongHexNumber = CTLongHexNumber.Factory.newInstance();
        ctLongHexNumber.setVal(bytes);

        // Create ctAbstractNum and setup
        ctAbstractNum = CTAbstractNum.Factory.newInstance();
        ctAbstractNum.setAbstractNumId(id);
        ctAbstractNum.setNsid(ctLongHexNumber);
        ctAbstractNum.setTmpl(ctLongHexNumber);

        // Only need 1 level, no nested is required
        ctLvl = ctAbstractNum.addNewLvl();
        ctLvl.setIlvl(BigInteger.valueOf(0));
        ctLvl.setTentative("0");

        // Add start
        ctLvl.addNewStart().setVal(BigInteger.valueOf(1));
        // Add numFmt
        ctLvl.addNewNumFmt().setVal(STNumberFormat.UPPER_LETTER);
        // Add lvlText
        ctLvl.addNewLvlText().setVal("%1.");
        // Add lvlJc
        ctLvl.addNewLvlJc().setVal(STJc.LEFT);
        // Add pPr and setup
        ctpPr = ctLvl.addNewPPr();
        {
            // Add ind and setup
            ctInd = ctpPr.addNewInd();
            ctInd.setLeft("720");
            ctInd.setHanging("720");
        }
        // Add pPr and setup
        ctrpr = ctLvl.addNewRPr();
        {
            // Add new rFonts and setup
            ctFonts = ctrpr.addNewRFonts();
            ctFonts.setAscii("Times New Roman");
            ctFonts.setHAnsi("Times New Roman");
            ctFonts.setEastAsia("Times New Roman");
            ctFonts.setCs("Times New Roman");
            // Add new b
            ctrpr.addNewB();
            // Add new sz
            ctrpr.addNewSz().setVal("24");
            // Add new szCs
            ctrpr.addNewSzCs().setVal("24");
            // Add new i
            ctrpr.addNewI().setVal("0");
        }

        return ctAbstractNum;
    }
}