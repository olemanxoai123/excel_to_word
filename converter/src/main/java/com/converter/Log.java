package com.converter;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;

/**
 * Handle exception alert and log
 */
public class Log {
    /**
     * General exception handler
     * 
     * @param thread
     * @param throwable
     */
    public static void handleException(Thread thread, Throwable throwable) {
        // Get the last cause
        Throwable cause = getTheLastCause(throwable);
        
        if (Platform.isFxApplicationThread()) {
            showExceptionAlert(cause);
        } else {
            cause.printStackTrace();
        }
    }

    /**
     * Show exception alert
     * 
     * @param throwable
     */
    public static void showExceptionAlert(Throwable throwable) {
        // Create holder
        Alert alert = new Alert(AlertType.ERROR);
        VBox vbox = new VBox();
        Label message = new Label("Message: " + throwable.getMessage());
        BorderPane borderPane = new BorderPane();
        Label label = new Label("Stack trace:");
        Button button = new Button("Copy");
        TextArea textArea = new TextArea();
        String stackTrace = getStackTrace(throwable);

        // Setup
        alert.setTitle("EXCEPTION");
        alert.setHeaderText(throwable.getClass().getSimpleName());
        // Setup textarea
        textArea.setEditable(false);
        textArea.setText(stackTrace);
        // Setup message, label
        message.setWrapText(true);
        message.setMaxWidth(480);
        message.setStyle("-fx-font-weight: bolder");
        label.setStyle("-fx-font-weight: bolder");
        // Setup button to copy exception to clipboard
        button.setOnAction(new EventHandler<ActionEvent>() {
            // On copy click
            @Override
            public void handle(ActionEvent arg0) {
                // Create StringSelection
                StringSelection stringSelection = new StringSelection(stackTrace);
                // Set content for clipboard
                Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
            }
        });
        button.setCursor(Cursor.HAND);

        // Add stack trace label and copy button to BorderPane
        borderPane.setLeft(label);
        borderPane.setRight(button);

        // Setup borderpane
        BorderPane.setAlignment(label, Pos.BOTTOM_LEFT);
        BorderPane.setMargin(label, new Insets(0, 0, 10, 0));
        BorderPane.setMargin(button, new Insets(0, 0, 10, 0));

        // Add label and textarea to vbox
        vbox.getChildren().addAll(message, new Label(""), borderPane, textArea);

        // Set vbox to alert content
        alert.getDialogPane().setContent(vbox);

        // Show alert
        alert.showAndWait();
    }

    /**
     * Get the last cause of a throwable
     * 
     * @param throwable
     * @return
     */
    public static Throwable getTheLastCause(Throwable throwable) {
        // Get cause
        Throwable cause = throwable.getCause();

        // If no more cause is found, throwable is the last
        if (cause == null) {
            return throwable;
        }

        // Reach the cause recursively
        return getTheLastCause(cause);
    }

    /**
     * Get the stack trace string
     * 
     * @param throwable
     * @return
     */
    public static String getStackTrace(Throwable throwable) {
        // Create resource
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);

        // Get stack trace
        throwable.printStackTrace(pw);

        return sw.toString();
    }
}
