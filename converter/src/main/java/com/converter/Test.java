package com.converter;

import exceptions.ValidationException;

public class Test {

    /**
     * Testing Converter
     *
     * @param args
     */
    public static void main(String[] args) {
        // Converter converter = new Converter("D:/My teacher/Program/test.xlsx", "D:/My teacher/Program/test.docx");
        try {
            Converter converter = new Converter("E:/Project/3. Other/excel_to_word/test.xlsx");
            converter.read();
            converter.shuffle(Converter.SHUFFLE_ALL);
            converter.run();
            converter.build("E:/Project/3. Other/excel_to_word/test.docx");
        } catch (ValidationException e) {
            e.printStackTrace();
        }
    }
}