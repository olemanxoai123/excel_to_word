package com.converter;

import java.util.Locale;
import java.util.ResourceBundle;

import com.google.common.base.CaseFormat;

public class Translator {
    // Bundle basename
    private static String baseName = "com.converter.locales.locale";
    // Determine localization
    private static Locale locale = new Locale("en", "US");
    // Resource Bundle
    private static ResourceBundle bundle = ResourceBundle.getBundle(baseName);
    // Debug mode
    private static boolean isDebug = false;

    //*********************
    //  Constant
    //*********************
    
    // Type field
    public static final Integer FIELD = 1;
    // Type lable
    public static final Integer LABEL = 2;
    // Type message
    public static final Integer MESSAGE = 3;
    // Supported language
    public static final String[][] SUPPORTED_LANGUAGES = {
        {"en_US", "English"},
        {"vi_VN", "Tiếng Việt"},
        {"ko_KR", "한국어"},
        {"ja_JP", "日本語"},
        {"zh_CN", "汉语"},
        {"fr_FR", "Français"},
        {"ru_RU", "русский язык"},
        {"es_ES", "Español"},
        {"it_IT", "Italiano"}
    };
    

    /**
     * Get locale
     * 
     * @return
     */
    public static String getLocale() {
        return locale.toString();
    }

    /**
     * Set locale
     * 
     * @param language
     */
    public static void setLocale(String language) {
        // Create holder
        String[] lang;

        // Retrive both language code and country code
        lang = language.split("_");
        // Set locale
        locale = new Locale(lang[0], lang[1]);
        // Set new bundle
        bundle = ResourceBundle.getBundle(baseName, locale);
    }

    /**
     * Get active language
     * 
     * @return
     */
    public static String[] getActiveLanguage() {
        // Get selected language
        String activeLanguage = getLocale();

        // Iterate through SUPPORTED_LANGUAGES
        for (String[] language : SUPPORTED_LANGUAGES) {
            if (activeLanguage.equals(language[0])) {
                return language;
            }
        }

        return null;
    }

    /**
     * Get bundle for auto localization
     * 
     * @return
     */
    public static ResourceBundle getBundle() {
        return bundle;
    }

    /**
     * Translate by key
     * 
     * @param key
     * @return
     */
    public static String __(String key) {
        // Create holder
        String translated;

        // Check if is in debug mode
        if (isDebug) {
            // No try catch
            translated = bundle.getString(key);
        } else {
            // Try catch
            try {
                // Get translated
                translated = bundle.getString(key);
            } catch (Throwable t) {
                // Set message empty
                translated = null;
            }
        }

        return translated;
    }

    /**
     * Translate by key and attr
     * 
     * @param key
     * @param attr
     * @return
     */
    public static String __(String key, String attr) {
        // Translate key and replace :attr with translated attr
        return __(key, attr, true);
    }

    /**
     * Translate by key and attr. If attr is plain string, just replace it in
     *
     * @param key
     * @param attr
     * @param isKey
     * @return
     */
    public static String __(String key, String attr, boolean isKey) {
        // Translate key and replace :attr with translated attr
        return __(key).replaceAll(":attr", isKey ? __(normalize(attr)) : attr);
    }

    /**
     * Translate by type and key
     * 
     * @param type
     * @param key
     * @return
     */
    public static String __(Integer type, String key) {
        // Create holder
        String prefix = "";

        // Check translation type
        if (type == FIELD) {
            prefix = "field";
        } else if (type == LABEL) {
            prefix = "label";
        } else if (type == MESSAGE) {
            prefix = "message";
        }

        return __(prefix + "." + key);
    }

    /**
     * Translate by type, key and attr
     * 
     * @param type
     * @param key
     * @param attr
     * @return
     */
    public static String __(Integer type, String key, String attr) {
        // Translate key and replace :attr with translated attr
        return __(type, key).replaceAll(":attr", __(normalize(attr)));
    }

    //*******************************
    // Private
    //*******************************

    private static String normalize(String key) {
        return "field." + CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, key);
    }
}
