package com.converter;

import com.converter.structure.BookCell;
import com.converter.structure.FormattedString;
import exceptions.*;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.stream.Collectors;

/**
 * Excel row validator
 */
public class Validator {

    public static void validateInput(HashMap<String, String> data) throws MissingRequiredInputException, MissingFileException {
        // Create holder
        String key, value;
        ArrayList<String> target = new ArrayList<>();

        // Iterate through map
        for (HashMap.Entry<String, String> entry : data.entrySet()) {
            key = entry.getKey();
            value = entry.getValue();

            // Check if field is not null or empty
            if (key.equals("namePrefix")) {
                continue;
            } else if (value == null || value.equals("")) {
                // Save target
                target.add(key);
            }
        }

        // Check if target exists
        if (!target.isEmpty()) {
            throw new MissingRequiredInputException(zip(target));
        }

        // Check if input exist
        folderCheck(data.get("from"), "from");
        // Check if output exist
        folderCheck(data.get("to"), "to");
    }

    /**
     * Validate data with Section type
     *
     * @param row
     * @param tracker
     * @return
     */
    public static ArrayList<FormattedString> validateSection(ArrayList<BookCell> row, int tracker) throws MissingRequiredColumnException, FillRedundantColumnException {
        // Validate data
        try {
            columnCheck(row, unzip("B"), null);
        } catch (ValidationException e) {
            // Set tracker
            e.setTracker(tracker);
            // Re-throw exception
            throw e;
        }

        // Collect values
        return collectValues(row);
    }

    /**
     * Va
     *
     * @param trackerlidate data with Question type
     * @param row
     * @return
     */
    public static ArrayList<FormattedString> validateQuestion(ArrayList<BookCell> row, int tracker) throws MissingRequiredColumnException, FillRedundantColumnException {
        // Validate data
        try {
            columnCheck(row, unzip("B, C, D, E, F, G"), null);
        } catch (ValidationException e) {
            // Set tracker
            e.setTracker(tracker);
            // Re-throw exception
            throw e;
        }

        // Collect values
        return collectValues(row);
    }

    /**
     * Validate data with Paragraph type
     *
     * @param row
     * @param tracker
     * @return
     */
    public static ArrayList<FormattedString> validateParagraph(ArrayList<BookCell> row, int tracker) throws MissingRequiredColumnException, FillRedundantColumnException {
        // Validate data
        try {
            columnCheck(row, unzip("B, C"), unzip("D, E, F, G"));
        } catch (ValidationException e) {
            // Set tracker
            e.setTracker(tracker);
            // Re-throw exception
            throw e;
        }

        // Collect values
        return collectValues(row);
    }

    /**
     * Validate data with Fill type
     *
     * @param row
     * @param tracker
     * @return
     */
    public static ArrayList<FormattedString> validateFill(ArrayList<BookCell> row, int tracker) throws MissingRequiredColumnException, FillRedundantColumnException {
        // Validate data
        try {
            columnCheck(row, unzip("B, C"), unzip("D, E, F, G"));
        } catch (ValidationException e) {
            // Set tracker
            e.setTracker(tracker);
            // Re-throw exception
            throw e;
        }

        // Collect values
        return collectValues(row);
    }

    /**
     * Validate data with Word type
     *
     * @param row
     * @param tracker
     * @return
     */
    public static ArrayList<FormattedString> validateWord(ArrayList<BookCell> row, int tracker) throws MissingRequiredColumnException, FillRedundantColumnException {
        // Validate data
        try {
            columnCheck(row, unzip("B"), null);
        } catch (ValidationException e) {
            // Set tracker
            e.setTracker(tracker);
            // Re-throw exception
            throw e;
        }

        // Collect values
        return collectValues(row);
    }

    /**
     * Validate data with Transformation type
     *
     * @param row
     * @param tracker
     * @return
     */
    public static ArrayList<FormattedString> validateTransformation(ArrayList<BookCell> row, int tracker) throws MissingRequiredColumnException, FillRedundantColumnException {
        // Validate data
        try {
            columnCheck(row, unzip("B, C"), null);
        } catch (ValidationException e) {
            // Set tracker
            e.setTracker(tracker);
            // Re-throw exception
            throw e;
        }

        // Collect values
        return collectValues(row);
    }

    /**
     * Validate data with Writing type
     *
     * @param row
     * @param tracker
     * @return
     */
    public static ArrayList<FormattedString> validateWriting(ArrayList<BookCell> row, int tracker) throws MissingRequiredColumnException, FillRedundantColumnException {
        // Validate data
        try {
            columnCheck(row, unzip("G"), null);
        } catch (ValidationException e) {
            // Set tracker
            e.setTracker(tracker);
            // Re-throw exception
            throw e;
        }

        // Collect values
        return collectValues(row);
    }

    /**
     * Validate data with Or type
     *
     * @param row
     * @param tracker
     * @return
     */
    public static ArrayList<FormattedString> validateOr(ArrayList<BookCell> row, int tracker) throws MissingRequiredColumnException, FillRedundantColumnException {
        // Validate data
        try {
            columnCheck(row, unzip("B, C, D, G"), null);
        } catch (ValidationException e) {
            // Set tracker
            e.setTracker(tracker);
            // Re-throw exception
            throw e;
        }

        // Collect values
        return collectValues(row);
    }

    /**
     * Zip ArrayList to string
     *
     * @param list
     * @return
     */
    public static String zip(ArrayList<String> list) {
        return String.join(", ", list);
    }

    /**
     * Unzip string to ArrayList
     *
     * @param list
     * @return
     */
    public static ArrayList<String> unzip(String list) {
        return new ArrayList<>(Arrays.asList(list.split(", ")));
    }

    // *************************************************************
    // Private
    // *************************************************************

    /**
     * Common column fill check
     *
     * @param row
     * @param require
     * @param optional
     * @throws MissingRequiredColumnException
     * @throws FillRedundantColumnException
     */
    private static void columnCheck(
        ArrayList<BookCell> row,
        ArrayList<String> require,
        ArrayList<String> optional
    ) throws MissingRequiredColumnException, FillRedundantColumnException {
        // Create holder
        ArrayList<String> redundant = new ArrayList<>();
        String address;

        // Check if addresses is contained in row
        for (BookCell bookCell : row) {
            // Get address
            address = bookCell.getAddress().substring(0, 1);

            // Exclude address in require if exist
            if (!require.remove(address) && (optional == null || !optional.remove(address))) {
                redundant.add(address);
            }
        }

        // If require is not empty, throw MissingRequiredColumnException
        if (!require.isEmpty()) {
            throw new MissingRequiredColumnException(zip(require));
        }

        // If redundant is not empty, throw FillRedundantColumnException
        if (!redundant.isEmpty()) {
            throw new FillRedundantColumnException(zip(redundant));
        }
    }

    /**
     * Collect an ArrayList of value from BookCell
     *
     * @param row
     * @return
     */
    private static ArrayList<FormattedString> collectValues(ArrayList<BookCell> row) {
        // Wow, get stream from ArrayList, then using map on stream (with method reference), then collect to ArrayList
        return row.stream().map(BookCell::getValue).collect(Collectors.toCollection(ArrayList::new));
    }

    /**
     * Check if folder exists
     *
     * @param path
     * @param key
     */
    private static void folderCheck(String path, String key) throws MissingFileException {
        // Create holder
        File file = new File(path);

        if (!file.exists()) {
            throw new MissingFileException(key);
        }
    }
}
