package com.converter.pages;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.awt.Desktop;

import com.converter.*;

import exceptions.*;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.scene.control.*;
import org.apache.commons.io.FilenameUtils;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser.ExtensionFilter;

public class ConvertPageController extends PageController {
    // ****************************
    // JavaFX Elements
    // ****************************
    @FXML
    ToggleGroup shuffleToggleGroup;
    @FXML
    TextField fileSize;
    @FXML
    TextField input;
    @FXML
    TextField output;
    @FXML
    Button convertBtn;
    @FXML
    Button shakingBtn;

    /**
     * For some reason, thread stop kill all related resources of this class
     * To prevent that, this class must not be local in Thread
     */
    Converter converter;

    /**
     * Initialize
     */
    @FXML
    private void initialize() {
        setupToggleGroup();
        setupFileSize();
        setupIconizedButton();
    }

    // ****************************
    // Setup
    // ****************************

    /**
     * Setup toggle group
     */
    private void setupToggleGroup() {
        // Set toogle group listener
        shuffleToggleGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> arg0, Toggle arg1, Toggle arg2) {
                // Get selected radio button
                RadioButton rb = (RadioButton) shuffleToggleGroup.getSelectedToggle();

                // File size text input will be disable if r1 is choosen
                fileSize.setDisable(rb.getId().equals("r1"));
                if (fileSize.isDisable()) {
                    // Set file size to defaultl data if disabled
                    fileSize.setText("1");
                }
            }
        });
    }

    /**
     * Setup toggle group
     */
    private void setupFileSize() {
        // Set default data if reach limit
        fileSize.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> arg0, String oldString, String newString) {
                if (newString.matches("^\\d{0,2}$")) {
                    if (newString.equals("0")) {
                        // Set to 1 if 0 is input
                        fileSize.setText("1");
                    }
                } else {
                    // If not match, revert to old string
                    if (newString.equals("")) {
                        // Set to 1 if this field is set blank
                        fileSize.setText("1");
                    } else {
                        // Revert
                        fileSize.setText(oldString);
                    }
                }
            }
        });
    }

    /**
     * Setup iconized button
     */
    private void setupIconizedButton() {
        // Set icon into button
        convertBtn.setGraphic(App.getImageView("convert", 20));
        shakingBtn.setGraphic(App.getImageView("dance", 20));
    }

    /**
     * Extract input data from controls
     */
    private HashMap<String, String> extractInput() {
        // Create holder
        HashMap<String, String> data = new HashMap<>();

        // Get data from input file browse
        String inputText = input.getText();
        // Set data relate to input file browse
        data.put("from", inputText);
        data.put("namePrefix", FilenameUtils.getBaseName(inputText));

        // Get data from output file browse
        data.put("to", output.getText());

        // Get selected toggle
        RadioButton radioButton = (RadioButton) shuffleToggleGroup.getSelectedToggle();
        // Set data relate to select toggle
        data.put("shuffleOption", radioButton.getId().substring(1));

        // Get data from shuffle file size
        data.put("fileSize", fileSize.getText());

        return data;
    }

    // ****************************
    // FXML event handlers
    // ****************************

    /**
     * Handle browse input file event
     *
     * @param event
     */
    @FXML
    private void onBrowseInput(ActionEvent event) throws Exception {
        // Create holder
        ArrayList<ExtensionFilter> extFilters = new ArrayList<>();

        // Create extension filters
        extFilters.add(new ExtensionFilter("Excel Open Xml Format", "*.xlsx"));

        // Select file
        File selectedFile = openFileChooser("Select a WORD file", "INPUT_FOLDER", extFilters);

        if (selectedFile != null) {
            // Set path to text field
            input.setText(selectedFile.getAbsolutePath());
            // Update OUTPUT_FOLDER
            Config.set("INPUT_FOLDER", selectedFile.getAbsolutePath());
        }
    }

    /**
     * Handle browse output file event
     *
     * @param event
     */
    @FXML
    private void onBrowseOutput(ActionEvent event) {
        // Select directory
        File selectedDirectory = openDirectoryChooser("Choose output folder", "OUTPUT_FOLDER");

        if (selectedDirectory != null) {
            // Set path to text field
            output.setText(selectedDirectory.getAbsolutePath());
            // Update OUTPUT_FOLDER
            Config.set("OUTPUT_FOLDER", selectedDirectory.getAbsolutePath());
        }
    }

    /**
     * Handle convert event
     *
     * @param event
     */
    @FXML
    private void onConvert(ActionEvent event) throws MissingRequiredColumnException, FillRedundantColumnException {
        Task<Void> task = new Task<>() {
            @Override
            protected Void call() throws Exception {
                // Convert main logic
                convert();

                return null;
            }
        };

        // Handle onConvert starting event
        task.setOnRunning(ConvertPageController::onConvertStart);
        // Handle onConvert failed event
        task.setOnFailed(ConvertPageController::onConvertFailed);
        // Handle onConvert succeeded event
        task.setOnSucceeded(ConvertPageController::onConvertSucceeded);

        // Start thread
        startThread(task);
    }

    /**
     * Show waiting alert with shaking duck
     *
     * @param event
     */
    @FXML
    private void onShaking(ActionEvent event) {
        showWaitingAlert();
    }

    /**
     * On open folder event handler
     *
     * @param event
     * @throws IOException
     */
    @FXML
    private void onOpenFolder(ActionEvent event) throws IOException {
        // Get event id
        String id = ((Button) event.getSource()).getId();
        String path = "";

        // Check if open input or output
        if (id.equals("openInput")) {
            path = input.getText();
        } else if (id.equals("openOutput")) {
            path = output.getText();
        }

        // Get File instance
        File file = new File(path);

        // Validate file instance
        if (file.exists()) {
            // Check if this is a file, not a directory
            if (file.isFile()) {
                file = new File(FilenameUtils.getFullPathNoEndSeparator(path));
            }

            // Open file from explorer
            Desktop.getDesktop().open(file);
        } else {
            // Show warning if folder is not exist
            showWarningAlert(Translator.__(Translator.MESSAGE, "folder_not_exist"));
        }
    }

    // ****************************
    // Private handler
    // ****************************

    /**
     * Handle onConvert starting event
     *
     * @param wse
     */
    private static void onConvertStart(WorkerStateEvent wse) {
        // Show waiting dialog and if waiting dialog is closed, terminate converting thread
        showWaitingAlert();
    }

    /**
     * Handle onConvert failed event
     *
     * @param wse
     */
    private static void onConvertFailed(WorkerStateEvent wse) {
        // Call Log to log exception
        try {
            // Terminate thread and close alert
            onConvertSucceeded(null);
            // Throw exception to re-catch
            throw wse.getSource().getException();
        } catch (ValidationException e) {
            showValidationErrorAlert(e);
            // Debug
            e.printStackTrace();
        } catch (ThreadDeath e) {
            System.out.println("ThreadDeath at ConvertPageController.java");
        } catch (Throwable e) {
            Log.showExceptionAlert(e);
        }
    }

    /**
     * Handle onConvert succeeded event
     *
     * @param wse
     */
    private static void onConvertSucceeded(WorkerStateEvent wse) {
        // Terminate thread
        terminateThread();
        // Close waiting alert
        closeWaitingAlert();
    }

    // ****************************
    // Private logic method
    // ****************************

    /**
     * Convert logic method
     *
     * @throws MissingRequiredColumnException
     * @throws FillRedundantColumnException
     */
    private void convert() throws ValidationException {
        // Create holder
        int size;
        String destination, newFile;
        // Extract data from controls
        HashMap<String, String> data = extractInput();

        // Validate input
        Validator.validateInput(data);

        // Call Converter
        converter = new Converter(data.get("from"));
        // Analyze data
        converter.read();

        // Get file size
        size = Integer.parseInt(data.get("fileSize"));
        // Get destination
        destination = data.get("to") + File.separator + data.get("namePrefix");
        // Do shuffling
        for (int i = 1; i <= size; i++) {
            if (size > 1) {
                // Add indexer
                newFile = destination + "_" + i;
            } else {
                newFile = destination;
            }
            // Add extension
            newFile += "_" + System.currentTimeMillis() + ".docx";

            // Run the converter
            converter.run();
            // Build (AKA Convert)
            converter.build(newFile);
            if (i < size - 1) {
                // Shuffle
                converter.shuffle(Integer.parseInt(data.get("shuffleOption")));
            }
        }
    }
}
