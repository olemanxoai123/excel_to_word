package com.converter.pages;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;

import com.converter.App;
import com.converter.Translator;

import org.apache.commons.io.FileUtils;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;

public class HelpPageController extends PageController {
    @FXML
    TreeView<String> typeTree;
    
    /** Note size */
    String[] prefixes = {"explain", "format", "note_first", "note_second", "note_third"};
    /** Tree item key */
    String[] keys = {"section", "question", "or", "word", "transformation", "paragraph", "fill", "writing"};

    /**
     * Initialize
     */
    @FXML
    private void initialize() {
        setupTreeView();
        setupScrollPane();
    }

    // ****************************
    // Setup
    // ****************************
    /**
     * Setup tree view
     */
    private void setupTreeView() {
        // Create holder
        TreeItem<String> root = new TreeItem<>();
        TreeItem<String> node;
        TreeItem<String> leaf;
        String message;

        // Iterate through note sizes
        for (String key : keys) {
            // Create type node
            node = new TreeItem<>("");
            // Add graphic
            node.setGraphic(
                // Graphic box
                makeGraphicBox(
                    App.getImageView(key, 24), 
                    "<b>" + Translator.__(keyOf(key, "label"))
                )
            );

            // Add note leaf. Number of leaf will be determine in sizes
            for (String prefix : prefixes) {
                // Create leaf
                leaf = new TreeItem<>("");
                // Get translated message
                message = Translator.__(keyOf(key, prefix));
                // Check if message is null
                if (message == null) {
                    continue;
                }
                // Preprocess message and generate graphic box
                leaf.setGraphic(makeGraphicBoxFromLeafMessage(message));
                // Add leaf to node
                node.getChildren().add(leaf);
            }

            // Add node to root
            root.getChildren().add(node);
        }

        // Setup tree
        typeTree.setShowRoot(false);
        typeTree.setEditable(false);
        typeTree.setRoot(root);
    }

    /**
     * Get translator key
     * 
     * @param index
     * @param prefix
     * @return
     */
    private String keyOf(String key, String prefix) {
        return "label.type" + "_" + key + "_" + prefix;
    }

    /**
     * Preprocess tree item message (leaf message). Add bold and underline for phrase behind ": "
     * 
     * @param message
     * @return
     */
    private HBox makeGraphicBoxFromLeafMessage(String message) {
        // Create holder
        HBox hBox;
        String[] parts;
        ImageView bullet = App.getImageView("bullet", 16);
        ArrayList<String> contents = new ArrayList<>();
    
        // Chop translated message for seperate Label creation
        parts = message.split(" : ");

        // Add first part (always)
        contents.add(parts[0]);
        // Check for parts size 
        if (parts.length != 1) {
            // Add ": " for the first part
            contents.set(0, contents.get(0) + ": ");
            // Split based on ", "
            for (String text : parts[1].split(", ")) {
                // Add text and define format (bold, underline)
                contents.add("<b><u>" + text);
                // Add ", "
                contents.add(", ");
            }

            // Remove the last redundant ", "
            contents.remove(contents.size() - 1);
        }

        // Make graphic box with predefine format (bold, underline)
        hBox = makeGraphicBox(bullet, Arrays.copyOf(contents.toArray(), contents.size(), String[].class));

        return hBox;
    }

    /**
     * Make a graphic box for TreeItem
     * 
     * @return
     */
    private HBox makeGraphicBox(ImageView imageView, String... contents) {
        // Create holder
        HBox hBox = new HBox();
        Label label;
        String text;

        // if name exists, add ImageView
        if (imageView != null) {
            // Add image view to HBox
            hBox.getChildren().add(imageView);
        }

        // Add label
        for (String content : contents) {
            // Create label
            label = new Label();
            // Get content
            text = content;

            // Check if content need to be bolder
            if (content.contains("<b>")) {
                // Create label
                text = text.replace("<b>", "");
                // Setup label
                label.setStyle(label.getStyle() + "-fx-font-weight: bolder;");
            }
            
            // Check if content need to be underline
            if (content.contains("<u>")) {
                // Create label
                text = text.replace("<u>", "");
                // Setup label
                label.setStyle(label.getStyle() + "-fx-underline: true;");
            }
            
            // Set label text
            label.setText(text);
            // Add label to box
            hBox.getChildren().add(label);
        }

        return hBox;
    }

    /**
     * Handle on download template click event
     * 
     * @param event
     * @throws IOException
     * @throws URISyntaxException
     */
    @FXML
    private void onDownloadTemplate(ActionEvent event) throws IOException, URISyntaxException {
        // Select directory
        File selectedDirectory = openDirectoryChooser("Save file at", "TEMPLATE_FOLDER");
        File downloadedFile = new File(selectedDirectory.getAbsolutePath() + File.separator + "template.xlsx");

        // Get template file
        InputStream templateFile = getResourceAsStream("/com/converter/template.xlsx");

        if (selectedDirectory != null) {
            // Save the template file
            FileUtils.copyInputStreamToFile(templateFile, downloadedFile);

            // Show information dialog
            showInfoAlert(Translator.__(Translator.MESSAGE, "download_successfully"));
        }
    }
}
