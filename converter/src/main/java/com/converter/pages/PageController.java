package com.converter.pages;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.converter.App;
import com.converter.Config;

import com.converter.Translator;
import com.converter.Validator;
import exceptions.ValidationException;
import javafx.concurrent.Task;
import javafx.event.EventHandler;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.util.Callback;
import org.apache.commons.io.FilenameUtils;

import javafx.fxml.FXML;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

public class PageController {
    // ****************************
    // JavaFX Elements
    // ****************************
    @FXML
    private VBox vBox;
    @FXML
    private ScrollPane scrollPane;

    // ****************************
    // Other resources
    // ****************************
    /**
     * Slave thread to run tasks
     */
    protected static Thread thread;
    /**
     * Slave to run waiting alert
     */
    private static Alert waitingAlert;

    /**
     * Information icon
     */
    private static final ImageView informationIcon = App.getImageView("info");
    /**
     * Error icon
     */
    private static final ImageView errorIcon = App.getImageView("cross");
    /**
     * Warning icon
     */
    private static final ImageView warningIcon = App.getImageView("warning");

    /**
     * Setup controller resources
     */
    public static void setup() {
        setupWaitingAlert();
    }

    /**
     * Create new scroll pane and disable the focus on click
     */
    protected void setupScrollPane() {
        // Create new scrollPane without requestFocus
        scrollPane = new ScrollPane(scrollPane.getContent()) {
            /**
             * This ***ing method should never be implement for this ****fing class
             */
            @Override
            public void requestFocus() {
            }
        };

        // Setup scrollPane
        scrollPane.setHbarPolicy(ScrollBarPolicy.NEVER);
        scrollPane.setVbarPolicy(ScrollBarPolicy.ALWAYS);
        scrollPane.setFocusTraversable(false);

        // Remove old scrollPane in vBox
        vBox.getChildren().remove(1);
        // Replace this scrollPane to vBox
        vBox.getChildren().add(scrollPane);
    }

    /**
     * Start a thread
     *
     * @param task
     */
    protected static void startThread(Task<Void> task) {
        // Create new thread
        thread = new Thread(task);
        // Start thread
        thread.start();
    }

    /**
     * Terminate a thread
     */
    protected static void terminateThread() {
        // Check if thread is alive
        if (thread != null && thread.isAlive()) {
            // Only interrupt if thread is alive
            try {
                thread.stop();
                // Clear thread
                thread = null;
            } catch (Exception e) {
                // Exception cause by interruption should not be thrown out
                e.printStackTrace();
            }
        }
    }

    /**
     * Show error alert
     *
     * @param content
     */
    protected static void showErrorAlert(String content) {
        // Create Alert
        Alert alert = createBasicAlert(AlertType.ERROR, "ERROR", content);

        // Show alert
        alert.showAndWait();
    }

    protected static void showValidationErrorAlert(ValidationException e) {
        // Create Alert
        Alert alert = createBasicAlert(AlertType.ERROR, "Something went wrong!", e.getDetailMessage());

        // Get message based on exception
        alert.setHeaderText(e.getShortMessage());

        // Show alert
        alert.showAndWait();
    }

    /**
     * Show info error
     *
     * @param content
     */
    protected static void showInfoAlert(String content) {
        // Create Alert
        Alert alert = createBasicAlert(AlertType.INFORMATION, "INFORMATION", content);

        // Show alert
        alert.showAndWait();
    }

    /**
     * Show warning alert
     *
     * @param content
     */
    protected static void showWarningAlert(String content) {
        // Create Alert
        Alert alert = createBasicAlert(AlertType.WARNING, "WARNING", content);

        // Show alert
        alert.showAndWait();
    }

    /**
     * Show confirm alert
     *
     * @param content
     * @return
     */
    protected static boolean showConfirmAlert(String content) {
        // Create holder
        Alert alert = createBasicAlert(AlertType.CONFIRMATION, "NOTICE", content);
        Optional<ButtonType> option = alert.showAndWait();

        return option.get().equals(ButtonType.OK);
    }

    /**
     * Show waiting alert for page thread
     */
    protected static void showWaitingAlert() {
        // Set onCloseRequest handler
        waitingAlert.setOnCloseRequest(PageController::onCancelWaitingAlert);
        // Show alert
        waitingAlert.show();
    }

    /**
     * Close waiting alert
     */
    protected static void closeWaitingAlert() {
        // Clear handler
        waitingAlert.setOnCloseRequest(null);
        // Close alert
        waitingAlert.close();
    }

    /**
     * Open file chooser
     *
     * @param title
     * @param configKey
     * @param extFilters
     * @return
     */
    protected static File openFileChooser(String title, String configKey, ArrayList<ExtensionFilter> extFilters) {
        // Create holder
        FileChooser fileChooser = new FileChooser();
        String path;
        File defaultDirectory;

        // Set title
        fileChooser.setTitle(title);
        // Filter extension
        fileChooser.getExtensionFilters().addAll(extFilters);

        if (Config.isSet(configKey)) {
            // Get path only
            path = FilenameUtils.getFullPath(Config.get(configKey));
            // Get file instance
            defaultDirectory = new File(path);
            // Check if directory is exist
            if (defaultDirectory.exists()) {
                // Set initial directory
                fileChooser.setInitialDirectory(defaultDirectory);
            }
        }

        return fileChooser.showOpenDialog(App.getWindow());
    }

    /**
     * Open directory chooser
     *
     * @param title
     * @param configKey
     * @return
     */
    protected static File openDirectoryChooser(String title, String configKey) {
        // Create holder
        DirectoryChooser directoryChooser = new DirectoryChooser();
        File defaultDirectory;

        // Set title
        directoryChooser.setTitle(title);

        if (Config.isSet(configKey)) {
            // Get default directory
            defaultDirectory = new File(Config.get(configKey));
            // Check if directory exist
            if (defaultDirectory.exists()) {
                // Set initial directory
                directoryChooser.setInitialDirectory(defaultDirectory);
            }
        }

        return directoryChooser.showDialog(App.getWindow());
    }

    /**
     * Get resource from App
     *
     * @param resource
     * @return
     */
    protected static InputStream getResourceAsStream(String resource) {
        return App.getResourceAsStream(resource);
    }

    // ****************************
    // Private
    // ****************************

    /**
     * Setup waiting alert
     */
    private static void setupWaitingAlert() {
        // Create holder
        ProgressBar progressBar = new ProgressBar();

        // Setup progressBar
        progressBar.setProgress(ProgressBar.INDETERMINATE_PROGRESS);
        progressBar.setPrefWidth(498);
        progressBar.setStyle("-fx-accent: #f1d338");

        // Create Alert
        waitingAlert = createBasicAlert(AlertType.NONE, "Please wait a sec!", null);
        // Add cancel button
        waitingAlert.getButtonTypes().add(new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE));
        // Setup content
        waitingAlert.getDialogPane().setContent(new VBox(
            new Label(Translator.__(Translator.MESSAGE, "waiting_alert")),
            new Label(),
            App.getImageView("dancing", "gif"),
            new Label(),
            progressBar
        ));
    }

    /**
     * Change the current scene
     *
     * @param event
     * @throws IOException
     */
    @FXML
    private void onChangeScene(MouseEvent event) throws IOException {
        // Get scene based on id
        String scene = ((Label) event.getSource()).getId();
        // Set scene root
        App.setRoot(scene);
    }

    /**
     * Handle onCancel waiting alert
     *
     * @param dialogEvent
     */
    private static void onCancelWaitingAlert(DialogEvent dialogEvent) {
        try {
            // Hanging thread for confirmation
            if (thread != null) {
                thread.suspend();
            }

            // Show confirm alert
            if (showConfirmAlert("Are you sure ?")) {
                // If confirmed, terminate thread
                terminateThread();
            } else {
                // Else, continue thread
                if (thread != null) {
                    thread.resume();
                    dialogEvent.consume();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Create basic alert object
     *
     * @param type
     * @param title
     * @param contentText
     * @return
     */
    private static Alert createBasicAlert(AlertType type, String title, String contentText) {
        // Create alert
        Alert alert = new Alert(type);

        // Setup alert
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(contentText);
        alert.initOwner(App.getWindow());

        // Set graphic based on type
        if (AlertType.INFORMATION.equals(type) || AlertType.CONFIRMATION.equals(type)) {
            alert.setGraphic(informationIcon);
        } else if (AlertType.ERROR.equals(type)) {
            alert.setGraphic(errorIcon);
        } else if (AlertType.WARNING.equals(type)) {
            alert.setGraphic(warningIcon);
        }

        return alert;
    }
}
