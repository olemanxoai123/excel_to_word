package com.converter.pages;

import java.io.IOException;

import com.converter.App;
import com.converter.Config;
import com.converter.Translator;
import com.google.common.base.Strings;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.util.Callback;

public class SettingPageController extends PageController {
    @FXML
    ComboBox<String[]> languageBox;
    @FXML
    CheckBox isBold;
    @FXML
    CheckBox isItalic;
    @FXML
    CheckBox isUnderline;
    @FXML
    ColorPicker fontColor;
    @FXML 
    ComboBox<String> wordSpaceCharBox;
    // WORD
    @FXML
    RadioButton wordSpaceUnderscore;
    @FXML
    RadioButton wordSpaceDot;
    @FXML
    RadioButton wordSpaceAfter;
    @FXML
    RadioButton wordSpaceBefore;
    // Fill
    @FXML
    RadioButton fillSpaceUnderscore;
    @FXML
    RadioButton fillSpaceDot;
    @FXML
    RadioButton fillSpaceAfter;
    @FXML
    RadioButton fillSpaceBefore;
    // Writing
    @FXML
    RadioButton writingSpaceUnderscore;
    @FXML
    RadioButton writingSpaceDot;

    // Example
    @FXML
    Label formatTextExample;
    @FXML
    Label colorExample;
    @FXML
    Label wordSpaceExample;
    @FXML
    Label fillSpaceExample;
    @FXML
    Label writingSpaceExample;

    /**
     * Initialize
     */
    @FXML
    private void initialize() {
        setupScrollPane();
        setupCombobox();
        setupPageData();
    }

    // ****************************
    // Setup
    // ****************************
    /**
     * Setup combobox
     */
    private void setupCombobox() {
        // Create callback for constructing new ListCell structure for combobox
        Callback<ListView<String[]>, ListCell<String[]>> callback = new Callback<>() {
            // Override constructor
            @Override
            public ListCell<String[]> call(ListView<String[]> arg0) {
                // Return new instance of ListCell
                return new ListCell<>() {
                    /**
                     * Override the updateItem to change the list item of combobox
                     * Item will be an array
                     * In each element of item will be another array with the size of 2
                     * [0] will contain locale code. [1] will contain message
                     * 
                     * @param item
                     * @param empty
                     */
                    @Override
                    protected void updateItem(String[] item, boolean empty) {
                        // Call super method
                        super.updateItem(item, empty);
                        
                        // Check if item is not null
                        if (item != null) {
                            // Set image
                            setGraphic(App.getImageView(item[0], 24));
                            // Set text
                            setText(item[1]);
                        }
                    };
                };
            }
        };

        // Set cell factory with new cell callback
        languageBox.setCellFactory(callback);
        // Set button cell with new cell callback
        languageBox.setButtonCell(callback.call(null));

        // Iterate through SUPPORTED_LANGUAGES to add to combobox
        languageBox.getItems().addAll(Translator.SUPPORTED_LANGUAGES);
        // Set active language as selected in combobox
        languageBox.setValue(Translator.getActiveLanguage());
    }

    /**
     * Setup Setting data based on Config
     */
    private void setupPageData() {
        // Setup checkbox
        isBold.setSelected(Boolean.parseBoolean(Config.get("DEFAULT_BOLD")));
        isItalic.setSelected(Boolean.parseBoolean(Config.get("DEFAULT_ITALIC")));
        isUnderline.setSelected(Boolean.parseBoolean(Config.get("DEFAULT_UNDERLINE")));
        // Display example
        displayFormatTextExample();

        // Setup color picker
        fontColor.setValue(Color.valueOf(Config.get("DEFAULT_COLOR")));

        //*******************************************************************
        // WORD
        //*******************************************************************
        // Set space char
        if (Config.get("WORD_SPACE_CHAR").equals("underscore")) {
            wordSpaceUnderscore.setSelected(true);
        } else if (Config.get("WORD_SPACE_CHAR").equals("dot")) {
            wordSpaceDot.setSelected(true);
        }
        // Set space pos
        if (Config.get("WORD_SPACE_POS").equals("after")) {
            wordSpaceAfter.setSelected(true);
        } else if (Config.get("WORD_SPACE_POS").equals("before")) {
            wordSpaceBefore.setSelected(true);
        }
        // Display example
        displaySpaceExample(
            wordSpaceExample, 
            wordSpaceUnderscore.isSelected() ? "underscore" : "dot", 
            wordSpaceAfter.isSelected() ? "after" : "before"
        );

        //*******************************************************************
        // FILL
        //*******************************************************************
        // Set space char
        if (Config.get("FILL_SPACE_CHAR").equals("underscore")) {
            fillSpaceUnderscore.setSelected(true);
        } else if (Config.get("FILL_SPACE_CHAR").equals("dot")) {
            fillSpaceDot.setSelected(true);
        }
        // Set space pos
        if (Config.get("FILL_SPACE_POS").equals("after")) {
            fillSpaceAfter.setSelected(true);
        } else if (Config.get("FILL_SPACE_POS").equals("before")) {
            fillSpaceBefore.setSelected(true);
        }
        // Display example
        displaySpaceExample(
            fillSpaceExample, 
            fillSpaceUnderscore.isSelected() ? "underscore" : "dot", 
            fillSpaceAfter.isSelected() ? "after" : "before"
        );

        //*******************************************************************
        // WORD
        //*******************************************************************
        // Set space char
        if (Config.get("WRITING_SPACE_CHAR").equals("underscore")) {
            writingSpaceUnderscore.setSelected(true);
        } else if (Config.get("WRITING_SPACE_CHAR").equals("dot")) {
            writingSpaceDot.setSelected(true);
        }
        // Display example
        displaySpaceExample(
            writingSpaceExample, 
            writingSpaceUnderscore.isSelected() ? "underscore" : "dot", 
            null
        );
        
    }

    // ****************************
    // Private
    // ****************************
    /**
     * Display example for format text
     */
    private void displayFormatTextExample() {
        // Create string builder
        StringBuilder sb = new StringBuilder();

        // Check if isBold
        if (isBold.isSelected()) {
            sb.append("-fx-font-weight: bolder;");
        }
        // Check if isItalic
        if (isItalic.isSelected()) {
            sb.append("-fx-font-style: italic;");
        }
        // Check if isUnderline
        if (isUnderline.isSelected()) {
            sb.append("-fx-underline: true");
        }

        // Set style
        formatTextExample.setStyle(sb.toString());
    }

    /**
     * Display example for word space
     */
    private void displaySpaceExample(Label label, String character, String pos) {
        // Create hlder
        StringBuilder sb = new StringBuilder(" (Example) ");
        String spaceChar = "";
        String space = "";

        // Check for char
        if (character.equals("underscore")) {
            spaceChar = "_";
        } else if (character.equals("dot")) {
            spaceChar = ".";
        }
        // Create space
        space = Strings.repeat(spaceChar, 5);
        // Check for position
        if (pos == null) {
            // Recreate if null
            sb = new StringBuilder(space);
        } else if (pos.equals("after")) {
            // Append if after
            sb.append(space);
        } else if (pos.equals("before")) {
            // Insert if before
            sb.insert(0, space);
        }

        // Set label
        label.setText(sb.toString().trim());
    }

    // ****************************
    // FXML event handlers
    // ****************************

    /**
     * Handle on save config
     *
     * @param event
     * @throws IOException
     */
    @FXML
    private void onSaveConfig(ActionEvent event) throws IOException {
        // Check if config is modified
        if (Config.isModified()) {
            // Confirm to save if config is modified
            if (showConfirmAlert(Translator.__("message.save_confirmation", "config_plural"))) {
                // Write config
                Config.write();
                // Read config (reload)
                Config.read();

                // Apply new locale
                Translator.setLocale(Config.get("LOCALE", "en_US"));
                // Reload scene
                App.setRoot("setting");
            }
        } else {
            // Warning if attempt to save without modify anything
            showWarningAlert(Translator.__(Translator.MESSAGE, "nothing_to_save"));
        }   
    }

    @FXML
    private void onChangeLanguage(ActionEvent event) {
        Config.set("LOCALE", languageBox.getValue()[0]);
    }

    /**
     * Handle on checkbox change
     * 
     * @param event
     */
    @FXML
    private void onChangeFormatText(ActionEvent event) {
        // Get source
        CheckBox source = (CheckBox) event.getSource();
        // Get config key based on source
        String key = "";

        // Check for source id
        if (source.getId().equals("c0")) {
            // Set config
            key = "DEFAULT_BOLD";
        } else if (source.getId().equals("c1")) {
            // Set config
            key = "DEFAULT_ITALIC";
        } else if (source.getId().equals("c2")) {
            // Set config
            key = "DEFAULT_UNDERLINE";
        }

        // Set config
        Config.set(key, Boolean.toString(source.isSelected()));
        // Display example
        displayFormatTextExample();
    }

    /**
     * Handle on change color picker event
     * 
     * @param event
     */
    @FXML
    private void onChangeColorPicker(ActionEvent event) {
        // Get color paint
        Paint paint = ((ColorPicker) event.getSource()).getValue();

        // Get color
        colorExample.setBackground(new Background(new BackgroundFill(paint, null, null)));

        // Set color Config
        Config.set("DEFAULT_COLOR", paint.toString().substring(2));
    }

    /**
     * Handle on change word space position event
     * No need to add event for ToggleGroup
     * 
     * @param event
     */
    @FXML
    private void onChangeSpace(ActionEvent event) {
        // Get radio button
        RadioButton radioButton = (RadioButton) event.getSource();
        // Analyze config data from id
        String[] parts = radioButton.getId().split(":");
        // Config key prefix
        String keyPrefix = parts[0].toUpperCase() + "_SPACE_";
        // Config key
        String key = keyPrefix + parts[1].toUpperCase();
        // Space object
        Label spaceExample;
        
        // Set config
        Config.set(key, parts[2]);

        // Get space object
        if (parts[0].equals("word")) {
            spaceExample = wordSpaceExample;
        } else if (parts[0].equals("fill")) {
            spaceExample = fillSpaceExample;
        } else {
            spaceExample = writingSpaceExample;
        }

        // Display example
        displaySpaceExample(spaceExample, Config.get(keyPrefix + "CHAR"), Config.get(keyPrefix + "POS"));
    }
}
