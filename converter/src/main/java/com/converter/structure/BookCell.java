package com.converter.structure;

/**
 * Contain cell detail
 */
public class BookCell {
    /**
     * Cell value
     */
    FormattedString value;
    /**
     * Cell address
     */
    String address;

    /**
     * Constructor
     *
     * @param address
     * @param value
     */
    public BookCell(String address, FormattedString value) {
        this.value = value;
        this.address = address;
    }

    /**
     * Get value
     *
     * @return
     */
    public FormattedString getValue() {
        return value;
    }

    /**
     * Get address
     *
     * @return
     */
    public String getAddress() {
        return address;
    }

    /**
     * Print BookCell
     */
    public void print() {
        System.out.println("{");
        System.out.println("    Address: " + address);
        System.out.println("    Value: " + value.toString());
        System.out.println("}");
    }
}
