package com.converter.structure;

import java.util.ArrayList;

/**
 * Represent single line. This one contain multiple CellsPack
 */
public class BookLine extends ArrayList<CellsPack> {
    // Constructor
    public BookLine() {
        super();
    }

    /**
     * Get last
     *
     * @return
     */
    public CellsPack getLast() {
        return get(size() - 1);
    }
}
