package com.converter.structure;

import java.awt.print.Book;
import java.util.ArrayList;

/**
 * Represent whole table. This one contain multiple LinesPack
 * Demonstration of structure: BookTable < LinesPack < BookLine < CellsPack < BookCell > > > >
 */
public class BookTable extends ArrayList<LinesPack> {
    /**
     * Level definition
     */
    static final String[] STRUCTURE = new String[]{"BookTable", "LinesPack", "BookLine", "CellsPack", "BookCell"};


    // Constructor
    public BookTable() {
        super();
    }

    /**
     * Print without unite table
     */
    public void print() {
        print(false);
    }

    /**
     * Print self data
     */
    public void print(boolean unpack) {
        if (unpack) {
            // Open  parenthese
            System.out.println("Table {");

            // Print recursively
            printUnpacked();

            // Close parenthese
            System.out.println("}");
        } else {
            // Open  parenthese
            System.out.println("BookTable {");

            // Print recursively
            printPackedRecursive(1, this);

            // Close parenthese
            System.out.println("}");
        }
    }

    /**
     * Unpack data into ArrayList of ArrayList of FormattedString (Line < Cell < String > > >)
     *
     * @return
     */
    public ArrayList<ArrayList<BookCell>> unpack() {
        // Create holder
        LinesPack linesPack;
        BookLine bookLine;
        CellsPack cellsPack;
        BookCell bookCell;

        // Create list
        ArrayList<ArrayList<BookCell>> line = new ArrayList<>();
        ArrayList<BookCell> cell;

        // Iterate through all lines pack
        for (int lpi = 0; lpi < size(); lpi++) {
            // Get lines pack
            linesPack = get(lpi);

            // Iterate through all book line
            for (int bli = 0; bli < linesPack.size(); bli++) {
                // Get cells pack
                bookLine = linesPack.get(bli);

                // Create new cell
                cell = new ArrayList<>();
                // Add cell to line
                line.add(cell);

                // Iterate through all cells pack
                for (int cpi = 0; cpi < bookLine.size(); cpi++) {
                    // Get cells pack
                    cellsPack = bookLine.get(cpi);

                    for (int fsi = 0; fsi < cellsPack.size(); fsi++) {
                        bookCell = cellsPack.get(fsi);
                        // Add formatted string to list
                        cell.add(bookCell);
                    }
                }
            }
        }

        return line;
    }

    // *************************
    // Private
    // *************************

    /**
     * Print out tab
     *
     * @param times
     */
    private void tab(int times) {
        for (int i = 0; i < times; i++) {
            System.out.print("  ");
        }
    }

    /**
     * Print out data recursively
     */
    @SuppressWarnings("unchecked")
    private void printPackedRecursive(int level, Object object) {
        // Create holder
        int size;
        ArrayList<Object> arrayList;

        //**************************
        // If level == 5, this is BookCell level
        //**************************
        if (level == 5) {
            // Casting
            BookCell bookCell = (BookCell) object;
            // Get String from BookCell
            String string = bookCell.getValue().toString();

            // Open bookCell
            System.out.print(bookCell.getAddress() + ": ");
            // Open string quote
            System.out.print("\"");

            // Check if string is to long, ... will be put in for replacement
            if (string.length() <= 50) {
                // Print out data
                System.out.print(string);
            } else {
                // Print ... instead
                System.out.print("...");
            }

            // Close string quote
            System.out.print("\"");

            // Stop condition
            return;
        }

        //**************************
        // If level < 5, this is above BookCell level
        //**************************

        // Uncheck cast
        arrayList = (ArrayList<Object>) object;
        // Get size
        size = arrayList.size();

        // Iterate through arrayList
        for (int i = 0; i < size; i++) {
            // Open parentheses
            tab(level);
            // CellsTable level will open their child (FormattedString) differently
            if (level == 4) {
                System.out.print(i + ": " + STRUCTURE[level] + " { ");
            } else {
                System.out.println(i + ": " + STRUCTURE[level] + " {");
            }

            // If level < 4, this is above FormattedString level
            printPackedRecursive(level + 1, arrayList.get(i));

            // CellsTable level will close their child (FormattedString) differently
            if (level == 4) {
                System.out.print(" ");
            } else {
                tab(level);
            }
            // Close parentheses
            System.out.println("}");
        }
    }

    /**
     * Print unpacked data
     */
    private void printUnpacked() {
        // Create holder
        ArrayList<ArrayList<BookCell>> list = unpack();
        ArrayList<BookCell> row;
        BookCell col;

        // Iterate through list
        for (int rowIndex = 0; rowIndex < list.size(); rowIndex++) {
            // Get row
            row = list.get(rowIndex);

            // Opening
            tab(1);
            System.out.println(rowIndex + ": Row { ");

            // Iterate through row
            for (int colIndex = 0; colIndex < row.size(); colIndex++) {
                // Get col
                col = row.get(colIndex);

                // Opening
                tab(2);
                // Open bookCell
                System.out.print(colIndex + ": Col { " + col.getAddress() + ": ");
                // Open string quote
                System.out.print("\"");

                if (col.toString().length() <= 50) {
                    // Print out data
                    System.out.print(col.getValue().toString());
                } else {
                    // Print ... instead
                    System.out.print("...");
                }

                // Close string quote
                System.out.println("\" }");
            }

            // Closing
            tab(1);
            System.out.println("}");
        }
    }
}
