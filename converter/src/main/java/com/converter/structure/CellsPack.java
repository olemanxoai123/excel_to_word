package com.converter.structure;

/**
 * Represent a multiple cell. This one contain multiple cell that merged together
 */
public class CellsPack extends Shuffleable<BookCell> {
    // Constructor
    public CellsPack() {
        super();
    }
}