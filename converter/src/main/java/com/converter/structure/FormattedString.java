package com.converter.structure;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * List of FormattedText
 */
public class FormattedString implements Iterable<FormattedText> { 
    // String string
    private ArrayList<FormattedText> texts;

    /**
     * Extract from RichTextString
     * 
     * @param string
     */
    public FormattedString() {
        // Default value
        this.texts = new ArrayList<>();
    }

    /**
     * Get text
     * 
     * @return
     */
    public FormattedText getText(int index) {
        return this.texts.get(index);
    }

    /**
     * Set text
     * 
     * @return
     */
    public void setText(int index, FormattedText text) {
        this.texts.set(index, text);
    }

    /**
     * Add a new formattedText
     * 
     * @param formattedText
     */
    public void addText(FormattedText formattedText) {
        this.texts.add(formattedText);
    }

    /**
     * Get plain string
     */
    public String toString() {
        // Create string builder
        StringBuilder sb = new StringBuilder();

        // Build a string without format
        for (FormattedText text : texts) {
            sb.append(text.toString());
        }

        return sb.toString();
    }

    /**
     * Iterator
     * 
     * @return
     */
    @Override
    public Iterator<FormattedText> iterator() {
        return this.texts.iterator();
    }
}