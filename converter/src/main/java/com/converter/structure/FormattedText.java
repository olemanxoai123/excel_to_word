package com.converter.structure;

/**
 * Text and text's format detail
 */
public class FormattedText {
    // String string
    private String string;
    // String color
    private String color;
    // String name
    private String font;
    // String size
    private double size;
    // String isBold
    private boolean isBold;
    // String isItalic
    private boolean isItalic;
    // String isUnderline
    private boolean isUnderline;

    /**
     * Extract from RichTextString
     * 
     * @param string
     */
    public FormattedText() {
        // Default value
        this.string = "";
        this.color = "00000000";
        this.font = "Times New Roman";
        this.size = 12;
        this.isBold = false;
        this.isItalic = false;
        this.isUnderline = false;
    }

    /**
     * Get string
     * 
     * @return
     */
    public String getString() {
        return this.string;
    }

    /**
     * Set string
     * 
     * @return
     */
    public void setString(String string) {
        this.string = string;
    }

    /**
     * Get color
     * 
     * @return
     */
    public String getColor() {
        return this.color;
    }

    /**
     * Set color
     * 
     * @return
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * Get font
     * 
     * @return
     */
    public String getFont() {
        return this.font;
    }

    /**
     * Set font
     * 
     * @return
     */
    public void setFont(String font) {
        this.font = font;
    }

    /**
     * Get size
     * 
     * @return
     */
    public double getSize() {
        return this.size;
    }

    /**
     * Set size
     * 
     * @return
     */
    public void setSize(double size) {
        this.size = size;
    }

    /**
     * Get isBold
     * 
     * @return
     */
    public boolean isBold() {
        return this.isBold;
    }

    /**
     * Set isBold
     * 
     * @return
     */
    public void setIsBold(boolean isBold) {
        this.isBold = isBold;
    }

    /**
     * Get isItalic
     * 
     * @return
     */
    public boolean isItalic() {
        return this.isItalic;
    }

    /**
     * Set isItalic
     * 
     * @return
     */
    public void setIsItalic(boolean isItalic) {
        this.isItalic = isItalic;
    }

    /**
     * Get isUnderline
     * 
     * @return
     */
    public boolean isUnderline() {
        return this.isUnderline;
    }

    /**
     * Set isUnderline
     * 
     * @return
     */
    public void setIsUnderline(boolean isUnderline) {
        this.isUnderline = isUnderline;
    }

    /**
     * toString
     */
    public String toString() {
        return this.string;
    }
}