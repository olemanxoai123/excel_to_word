package com.converter.structure;

/**
 * Represent multiple lines. This one contain multiple SingleLine that merged together
 */
public class LinesPack extends Shuffleable<BookLine> {
    // Constructor
    public LinesPack() {
        super();
    }
}