package com.converter.structure;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/**
 * Represnt a shuffleable list
 */
public class Shuffleable<E> extends ArrayList<E> {
    // Determine if these lines is shuffleable
    protected boolean isShuffleable;

    // Constructor
    public Shuffleable() {
        super();

        this.isShuffleable = false;
    }

    /**
     * Check if this is shuffleable
     * 
     * @return
     */
    public boolean isShuffleable() {
        return isShuffleable;
    }

    /**
     * Set shuffleable state
     * 
     * @param isShuffleable
     */
    public void setIsShuffleable(boolean isShuffleable) {
        this.isShuffleable = isShuffleable;
    }

    /**
     * Shuffle data
     */
    @SuppressWarnings("unchecked")
    public int shuffle(int preserve) {
        // Create holder
        ArrayList<Integer> indexes = new ArrayList<>();

        // Create an copyOf elements's indexes. Only index will be actually shuffle. So
        // then, we can detect where the original index is
        for (int i = 0; i < size(); i++) {
            // Add index based on size
            indexes.add(i);
        }

        // Create cloned index
        ArrayList<Integer> cloned = (ArrayList<Integer>) indexes.clone();
        // Shuffle cloned elements
        Collections.shuffle(cloned, new Random());

        // Iterate through cloned to add pseudo shuffle index
        for (int i = 0; i < cloned.size(); i++) {
            // Add the element with index contained in cloned.
            add(get(cloned.get(i)));
        }
        
        // Remove unshuffled data. New data will be append into the list. That mean
        // unshuffled (original) data will remain before shuffled data
        removeRange(0, size() / 2);

        // Return the preserve's new index
        return cloned.indexOf(preserve);
    }

    /**
     * Shuffle with no preservation
     */
    public void shuffle() {
        shuffle(0);
    }
}