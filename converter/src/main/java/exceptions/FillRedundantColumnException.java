package exceptions;

public class FillRedundantColumnException extends ValidationException {
    /**
     * Constructor
     *
     * @param message
     */
    public FillRedundantColumnException(String message) {
        super(message, false);
    }
}
