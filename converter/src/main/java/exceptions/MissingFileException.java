package exceptions;

public class MissingFileException extends ValidationException {
    /**
     * Constructor
     *
     * @param message
     */
    public MissingFileException(String message) {
        super(message, false);
    }
}
