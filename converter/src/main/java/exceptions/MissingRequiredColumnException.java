package exceptions;

import com.converter.Validator;

public class MissingRequiredColumnException extends ValidationException {
    /**
     * Constructor
     *
     * @param message
     */
    public MissingRequiredColumnException(String message) {
        super(message, false);
    }
}
