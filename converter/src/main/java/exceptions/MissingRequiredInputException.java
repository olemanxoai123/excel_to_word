package exceptions;

public class MissingRequiredInputException extends ValidationException {
    /**
     * Constructor
     *
     * @param message
     */
    public MissingRequiredInputException(String message) {
        super(message, false);
    }
}
