package exceptions;

import com.converter.Translator;
import com.converter.Validator;

import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * Parent exception class for validator's specific exception
 */
public class ValidationException extends Exception {
    /**
     * Exception is warning only
     */
    protected boolean isWarning;
    /**
     * Tracker
     */
    protected int tracker;
    /**
     * Full message template
     */
    protected String template;

    /**
     * Constructor
     *
     * @param message
     * @param isWarning
     */
    public ValidationException(String message, boolean isWarning) {
        super(message);
        // This is exception
        this.isWarning = isWarning;
    }

    /**
     * Check if this is warning only exception
     *
     * @return
     */
    public boolean isWarning() {
        return isWarning;
    }

    /**
     * Get tracker in document
     *
     * @return
     */
    public int getTracker() {
        return tracker;
    }

    /**
     * Set tracker
     *
     * @param tracker
     */
    public void setTracker(int tracker) {
        this.tracker = tracker;
    }

    /**
     * Get message from exception and convert it to column headers
     *
     * @return
     */
    public String getTarget() {
        return Validator.zip(
            Validator.unzip(getMessage())
                .stream()
                .map(item -> Translator.__(Translator.FIELD, item))
                .collect(Collectors.toCollection(ArrayList::new))
        );
    }

    /**
     * Get short, digest message
     *
     * @return
     */
    public String getShortMessage() {
        return Translator.__(Translator.MESSAGE, getClass().getSimpleName());
    }

    /**
     * Get detail message
     *
     * @return
     */
    public String getDetailMessage() {
        // Create holder
        String message = Translator.__(Translator.MESSAGE, getClass().getSimpleName() + "_template");

        // Replace target
        message = message.replace(":target", getTarget());
        // Replace tracker
        message = message.replace(":tracker", Integer.toString(getTracker()));

        return message;
    }
}
